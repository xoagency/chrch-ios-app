//
//  PhotoModel.swift
//  Instagram
//
//  Created by Isis Anchalee on 2/3/16.
//  Copyright © 2016 Isis Anchalee. All rights reserved.
//

import UIKit

class PhotoModel: NSObject {
    var url: String?
    var username: String?
    var profilePicture: String?
    var countLikes: String?
    var type: String?
    var uriVideo: String?

    
    init(json: NSDictionary){
        
        print(json)
        
        self.type = json["type"]  as? String
        
        if self.type == "video" {
            
            let videos = json["videos"] as? NSDictionary

            let standard_resolution_video = videos?["standard_resolution"] as? NSDictionary
            
            let url = standard_resolution_video?["url"] as? String

            self.uriVideo = url
        }
        
        let image = json["images"] as? NSDictionary
        
        let standard_resolution = image?["standard_resolution"] as? NSDictionary
        
        let url = standard_resolution?["url"] as? String
        
        self.url = url
        
        
        let user = json["user"] as? NSDictionary
        
        self.username = user?["username"] as? String
        
        self.profilePicture = user?["profile_picture"] as? String

        let likes = json["likes"] as? NSDictionary
        let countLikes = likes?["count"] as? NSInteger
        
        self.countLikes = (countLikes?.toIntMax().description)! + " Me gusta"
    }
}
