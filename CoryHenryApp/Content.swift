//
//  Content.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/13/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class Content: NSObject {
    
    var id: String?
    var title: String?
    var subtitle: String?
    var image_url: String?
    var created_at: Date?
    var updated_at: Date?
    var premium: Bool?
    var item_type: Int?
    var item_id: String?
    var image: UIImage?
    var isVideo: Bool?
    var isEvent: Bool?
    var typeMedia: Int?
    var image_local: String?
}
