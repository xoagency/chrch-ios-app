//
//  PhotosTableViewCell.swift
//  Instagram
//
//  Created by Satoru Sasozaki on 2/2/16.
//  Copyright © 2016 Satoru Sasozaki. All rights reserved.
//

import UIKit

class PhotosTableViewCell: UITableViewCell {
    
    var photoView : PhotoView?
    var imageURL : URL?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        // Add photoView to this Cell class
        // Height and width are set to 375, because I'm testing on iPhone 6
        photoView = PhotoView(frame: CGRect(x: 0, y: 0, width: 375, height: 375))
        self.contentView.addSubview(photoView!)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    // Add function to make it easier to set image from URL
    func configureImage(_ imageURL : URL) {
        self.imageURL = imageURL
        photoView?.setImageWithUrl(imageURL)    
    }
}
