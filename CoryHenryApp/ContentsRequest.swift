//
//  ContentsRequest.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/13/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class ContentsRequest: BaseRequest {
    
    override func path() -> String {
        return "\(apiEndpointURL)/contents"
    }

}
