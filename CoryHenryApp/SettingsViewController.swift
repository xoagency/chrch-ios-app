//
//  SettingsViewController.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 1/4/17.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit
import MessageUI

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var aboutLabel: UILabel!
    
    
    // MARK: - Variables
    
    var supportController: MFMailComposeViewController? = nil
    
    
    // MARK: - Setup

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        // Configure about label
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String, let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            self.aboutLabel.text = "Version \(version) (\(build))\nApp by Recess University LLC\nDeveloped by Crown & Lantern LLC"
        }
        
        
        // Configure table view
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "SettingsTableViewCell", bundle: nil), forCellReuseIdentifier: "SettingsTableViewCell")
        self.tableView.separatorStyle = .none
        self.tableView.alwaysBounceVertical = false
        self.tableView.backgroundColor = BACKGROUND_COLOR_MAIN
        
        let footer = UIView()
        footer.backgroundColor = UIColor.clear
        self.tableView.tableFooterView = footer
        
        
        
        // Register for user state updates
        NotificationCenter.default.addObserver(self, selector: #selector(self.userStateChanged), name: USER_STATE_CHANGED_NOTIFICATION, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func dismissButtonAction(_ sender: Any) {
        if (self.navigationController != nil) {
            let _ = self.navigationController?.popViewController(animated: true)
        }
        else if (self.presentingViewController != nil) {
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - Table View
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (CURRENT_USER != nil) {
            // Show log out option
            return 3
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 3
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        
        if (indexPath.section == 0) {
            
            switch indexPath.row {
            case 0:
                if (CURRENT_USER != nil) {
                    cell.title = CURRENT_USER!.email!
                    cell.isButton = false
                } else {
                    cell.title = "Create an account / Sign in"
                    cell.isButton = true
                }
            case 1:
                cell.title = "Subscribe"
                cell.isButton = true
            default:
                cell.title = ""
            }
            
        }
        else if (indexPath.section == 1) {
            
            cell.isButton = true
            
            switch indexPath.row {
            case 0:
                cell.title = "Legal"
            case 1:
                cell.title = "FAQ"
            case 2:
                cell.title = "Support"
            default:
                cell.title = ""
            }
            
        }
        else if (indexPath.section == 2) {
            if (CURRENT_USER != nil) {
                cell.title = "Log out"
                cell.isButton = true
            }
            else {
                cell.title = ""
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (section == 0) {
            return 10.0
        }
        return 28.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if (indexPath.section == 0) { // Account/Subscribe
            
            switch indexPath.row {
            case 0:
                if (CURRENT_USER == nil) {
                    let accountPromptViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AccountPromptViewController") as! AccountPromptViewController
                    self.present(accountPromptViewController, animated: true, completion: nil)
                }
            case 1:
                let subscribeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SubscribeViewController") as! SubscribeViewController
                self.present(subscribeViewController, animated: true, completion: nil)
            default:
                break
            }
            
        }
        else if (indexPath.section == 1) { // Legal/FAQ/Support
            switch indexPath.row {
            case 2:
                self.promptSupportEmail()
                return
            default:
                break
            }
        }
        else if (indexPath.section == 2) { // Log out
            self.logOut()
        }
        
    }
    
    
    
    // MARK: - Support
    
    func promptSupportEmail() {
        
        if (MFMailComposeViewController.canSendMail()) {
            
            var appVersion = ""
            if let v = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String, let b = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
                appVersion = "Version \(v) (\(b))"
            }
            let osVersion = UIDevice.current.systemVersion
            let deviceModel = UIDevice.current.model
            
            self.supportController = MFMailComposeViewController()
            self.supportController!.mailComposeDelegate = self
            
            self.supportController!.setToRecipients(["support@coryhenry.com"])
            self.supportController!.setSubject("Support (iOS)")
            self.supportController!.setMessageBody("App Version: \(appVersion)\nHardware: \(deviceModel)\nSystem Version: \(osVersion)\n\n\n", isHTML: false)
            
            self.present(self.supportController!, animated: true, completion: nil)
            
        }
        else {
            
            let alert = UIAlertController(title: "Oops!", message: "Please check your email configuration and try again.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
            
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    // MARK: - User State Changed
    func userStateChanged() {
        self.tableView.reloadData()
    }
    
    
    
    // MARK: - Log Out
    func logOut() {
        
        CURRENT_USER = nil
        AUTH_TOKEN = nil
        
        saveUserState()
        NotificationCenter.default.post(name: USER_STATE_CHANGED_NOTIFICATION, object: nil)
    }
    

}
