//
//  ContentItemRequest.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/13/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class ContentItemRequest: BaseRequest {
    
    var item_type: Int
    var item_id: String
    
    init(item_type: Int, item_id: String) {
        self.item_type = item_type
        self.item_id = item_id
    }
    
    override func path() -> String {
        return "\(apiEndpointURL)/contents/item/\(self.item_type)/\(self.item_id)"
    }

}
