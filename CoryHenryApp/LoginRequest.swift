//
//  LoginRequest.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 1/1/17.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit
import Alamofire

class LoginRequest: BaseRequest {
    
    var email: String
    var password: String
    
    init(email: String, password: String) {
        self.email = email
        self.password = password
    }
    
    override func path() -> String {
        return "\(apiEndpointURL)/users/login"
    }
    
    override func method() -> HTTPMethod {
        return .post
    }
    
    override func params() -> [String : Any]? {
        var params: [String: Any] = [:]
        params["email"] = self.email
        params["password"] = self.password
        return params
    }

}
