//
//  AccountPromptViewController.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 1/1/17.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit

class AccountPromptViewController: UIViewController {
    
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!

    @IBOutlet weak var backgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        // Configure buttons
        self.loginButton.layer.borderColor = UIColor.white.cgColor
        self.loginButton.layer.borderWidth = 1
        
        
        // Configure background
        /*
        let random = Int(arc4random_uniform(UInt32(3)))
        switch random {
        case 0:
            backgroundImageView.image = UIImage(named: "BackgroundImage0")
        case 1:
            backgroundImageView.image = UIImage(named: "BackgroundImage1")
        case 2:
            backgroundImageView.image = UIImage(named: "BackgroundImage2")
        default:
            break
        }
 */
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    // MARK: - Actions
    
    @IBAction func dismissButtonAction(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func registerButtonAction(_ sender: Any) {
        let registerViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.present(registerViewController, animated: true, completion: nil)
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        let loginViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(loginViewController, animated: true, completion: nil)
    }
    

}
