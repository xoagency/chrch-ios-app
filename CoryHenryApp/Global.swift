//
//  Global.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/23/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import Foundation
import UIKit
import TwitterKit
import TwitterCore

// MARK: - Global vars
var CURRENT_USER: User? = nil
var AUTH_TOKEN: String? = nil

var MUSIC_PLAYER: MusicPlayer? = nil

var TYPE_TWITTER: Int? = 0
var TYPE_INSTAGRAM: Int? = 1


var TYPE_WHATS: String? = "0"
var TYPE_VIDEO: String? = "1"
var TYPE_EVENT: String? = "2"
var TYPE_STORE: String? = "3"
var TYPE_ALBUM: String? = "4"
var TYPE_SOCIAL: String? = "5"
var TYPE_BLOG: String? = "6"


// MARK: - Color scheme
let BACKGROUND_COLOR_MAIN = UIColor(red: 18.0/255.0, green: 19.0/255.0, blue: 20.0/255.0, alpha: 1.0)
let BACKGROUND_COLOR_SECONDARY = UIColor(red: 34.0/255.0, green: 35.0/255.0, blue: 38.0/255.0, alpha: 1.0)
let ACCENT_COLOR_MAIN = UIColor(red: 72.0/255.0, green: 188.0/255.0, blue: 201.0/255.0, alpha: 1.0)

let ACCENT_COLOR_BACKGROUND_CARD_TWITTER = UIColor(red: 32.0/255.0, green: 36.0/255.0, blue: 39.0/255.0, alpha: 1.0)

let ACCENT_COLOR_NAVIGATION_BAR = UIColor(red: 18.0/255.0, green: 19.0/255.0, blue: 20.0/255.0, alpha: 1.0)

let ACCENT_COLOR_BUTTONS = UIColor(red: 76.0/255.0, green: 170.0/255.0, blue: 181.0/255.0, alpha: 1.0)

let COLOR_DIVIDER = UIColor(red: 34.0/255.0, green: 35.0/255.0, blue: 38.0/255.0, alpha: 1.0)

// MARK: - Notification identifiers
let MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION = Notification.Name("MusicPlayerStateChangedNotification")
let USER_STATE_CHANGED_NOTIFICATION = Notification.Name("UserStateChangedNotification")

//MARK: - Key's Twiteer 
let TWITTER_COSUMMER_KEY    =  "y7Eq53vhs8NG8lVEkN0e3zF4U"
let TWITTER_COSUMMER_SECRET =  "oFWQBlS2ci4C2h384hMTrCMTxNFh63i5mbRZDIJDqSTYKZcWNj"

// MARK: - Bool <-> Int
extension Bool {
    init<T: Integer>(_ num: T) {
        self.init(num != 0)
    }
}

func intFromBool(bool: Bool) -> Int {
    if (bool) {
        return 1
    } else {
        return 0
    }
}


// MARK: - UIImage functions
func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage
{
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
    image.draw(in: CGRect(x: 0.0, y: 0.0, width: newSize.width, height: newSize.height))
    
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage!;
}


// MARK: - Date functions
func stringToDate(date: String) -> Date {
    let formatter = DateFormatter()
    
    // Format 1
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    if let parsedDate = formatter.date(from: date) {
        return parsedDate
    }
    
    // Format 2
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:SSSZ"
    if let parsedDate = formatter.date(from: date) {
        return parsedDate
    }
    
    // Couldn't parsed with any format. Just get the date
    let splitDate = date.components(separatedBy: "T")
    if splitDate.count > 0 {
        formatter.dateFormat = "yyyy-MM-dd"
        if let parsedDate = formatter.date(from: splitDate[0]) {
            return parsedDate
        }
    }
    
    // Nothing worked :(
    return Date()
}


// MARK: - Email validation
func isValidEmail(email: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let result = emailTest.evaluate(with: email)
    return result
}


// MARK: - User state
func saveUserState() {
    
    if (CURRENT_USER == nil) {
        UserDefaults.standard.set(nil, forKey: "CURRENT_USER.id")
        UserDefaults.standard.set(nil, forKey: "CURRENT_USER.email")
        UserDefaults.standard.set(nil, forKey: "CURRENT_USER.facebook_id")
        UserDefaults.standard.set(0, forKey: "CURRENT_USER.hasPassword")
    }
    else {
        UserDefaults.standard.set(CURRENT_USER!.id, forKey: "CURRENT_USER.id")
        UserDefaults.standard.set(CURRENT_USER!.email, forKey: "CURRENT_USER.email")
        UserDefaults.standard.set(CURRENT_USER!.facebook_id, forKey: "CURRENT_USER.facebook_id")
        UserDefaults.standard.set(intFromBool(bool: CURRENT_USER!.hasPassword), forKey: "CURRENT_USER.hasPassword")
    }
    
    UserDefaults.standard.set(AUTH_TOKEN, forKey: "AUTH_TOKEN")
    
    UserDefaults.standard.synchronize()
}

func loadUserState() {
        
    if (UserDefaults.standard.string(forKey: "CURRENT_USER.id") == nil) {
        CURRENT_USER = nil
    }
    else {

        let user = User()
        user.id = UserDefaults.standard.string(forKey: "CURRENT_USER.id")
        user.email = UserDefaults.standard.string(forKey: "CURRENT_USER.email")
        user.facebook_id = UserDefaults.standard.string(forKey: "CURRENT_USER.facebook_id")
        user.hasPassword = Bool(UserDefaults.standard.integer(forKey: "CURRENT_USER.hasPassword"))
        
        CURRENT_USER = user
    }
    
    AUTH_TOKEN = UserDefaults.standard.string(forKey: "AUTH_TOKEN")
}



















