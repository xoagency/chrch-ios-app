//
//  LoginViewController.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 1/2/17.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    
    // MARK: - Setup

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        // Configure text fields
        self.emailTextField.delegate = self
        self.emailTextField.text = nil
        self.emailTextField.textColor = UIColor.white
        self.emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName : UIColor.darkGray])
        
        self.passwordTextField.delegate = self
        self.passwordTextField.text = nil
        self.passwordTextField.textColor = UIColor.white
        self.passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : UIColor.darkGray])
        
        
        
        // Dismiss keyboard on tap
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tapRecognizer)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.dismissKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func dismissButtonAction(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func facebookButtonAction(_ sender: Any) {
        self.loginWithFacebook()
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        self.login()
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: - Text Fields
    
    func dismissKeyboard() {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == self.emailTextField) {
            self.emailTextField.resignFirstResponder()
            self.passwordTextField.becomeFirstResponder()
        } else if (textField == self.passwordTextField) {
            self.passwordTextField.resignFirstResponder()
        }
        
        return true
    }
    
    
    
    // MARK: - Email/Password Login
    
    func login() {
        
        self.dismissKeyboard()
        
        // Email validation
        if (self.emailTextField.text == nil || self.emailTextField.text! == "") {
            let alert = UIAlertController(title: "Oops!", message: "Invalid email or password.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        // Password validation
        if (self.passwordTextField.text == nil || self.passwordTextField.text! == "") {
            let alert = UIAlertController(title: "Oops!", message: "Invalid email or password.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        
        let email = self.emailTextField.text!
        let password = self.passwordTextField.text!
        
        // Log in
        FIRAuth.auth()?.signIn(withEmail: email, password: password) { (user, error) in
            
            if error == nil {
                
                //Print into the console if successfully logged in
                print("You have successfully logged in")
                
                
                let userCurrent = User()
                
                userCurrent.id = user?.uid
                userCurrent.email = user?.email
                userCurrent.hasPassword = true
                
                let token = user?.uid
                
                // Update user state
                
                CURRENT_USER = userCurrent
                AUTH_TOKEN = token
                
                saveUserState()
                NotificationCenter.default.post(name: USER_STATE_CHANGED_NOTIFICATION, object: nil)
                
                self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                
            } else {
                
                let alert = UIAlertController(title: "Oops!", message: "\(error!)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return

            }
        }

        /*
        let request = LoginRequest(email: email, password: password)
        request.completionBlock = { (response, error) in
            self.onLogin(response: response, error: error)
        }
        request.execute()
        */
        
    }
    
    
    
    // MARK: - Facebook Login
    
    func loginWithFacebook() {
        
        self.dismissKeyboard()
        
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (loginResult: FBSDKLoginManagerLoginResult?, error: Error?) in
            
            if (error != nil || loginResult == nil) {
                
                print("Facebook login error: \(error)")
                
                let alert = UIAlertController(title: "Oops!", message: "Something went wrong. Please try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            else if (loginResult!.isCancelled) {
                print("Facebook login cancelled.")
                return
            }
            else {
                print("Facebook login successful.")
                
                FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"email, id"]).start(completionHandler: { (connection, result, error) in
                    
                    if let result = result as? NSDictionary {
                        
                        let id = result["id"] as? String
                        let token = FBSDKAccessToken.current().tokenString
                        
                        // Log In
                        let request = LoginFacebookRequest(facebook_id: id!, facebook_token: token!)
                        request.completionBlock = { (response, error) in
                            self.onLogin(response: response, error: error)
                        }
                        request.execute()
                        
                    }
                    else {
                        
                        print("Facebook login error: \(error)")
                        
                        let alert = UIAlertController(title: "Oops!", message: "Something went wrong. Please try again.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        return
                    }
                    
                })
                
                
            }
            
        }
        
    }
    
    
    
    // Mark: - Handle Response
    
    func onLogin(response: JSON?, error: Any?) {
        
        if (error != nil) {
            
            let alert = UIAlertController(title: "Oops!", message: "\(error!)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        
        // Parse response
        if let json = response {
            
            let user = User()
            if (json["user"] != JSON.null) {
                user.id = json["user"]["_id"].string
                user.email = json["user"]["email"].string
                user.facebook_id = json["user"]["facebook_id"].string
                if (json["user"]["password"] != JSON.null) {
                    user.hasPassword = true
                }
            }
            
            let token = json["token"].string
            
            // Error check
            if (user.id == nil || user.email == nil || token == nil) {
                let alert = UIAlertController(title: "Oops!", message: "Something went wrong. Please try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            
            // Update user state
            
            CURRENT_USER = user
            AUTH_TOKEN = token
            
            saveUserState()
            NotificationCenter.default.post(name: USER_STATE_CHANGED_NOTIFICATION, object: nil)
            
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            
        }
        
    }
    

}
