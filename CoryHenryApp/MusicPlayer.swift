//
//  MusicPlayer.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/23/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

protocol MusicPlayerPlaybackDelegate : NSObjectProtocol {
    func currentTrackPlaybackTimeChanged(currentTrackTime: Int)
}

class MusicPlayer: NSObject {
    
    var album: Album?
    
    var current_track_index: Int?
    var playQueue: [Int] = []
    
    private var players: [AVPlayer] = []
    
    private var currentTrackTimeObserver: Any? = nil
    
    weak var playbackDelegate: MusicPlayerPlaybackDelegate?
    
    // MARK: - Configuration
    
    override init() {
        super.init()
        
        // Configure audio settings
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            do {
                try AVAudioSession.sharedInstance().setActive(true)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        // Enable remote commands
        self.enableRemoteCommands()

        
        // Load default
        self.loadDefaultAlbum()
    }
    
    func prepareToPlay(album: Album, track_index: Int) {
        
        if (self.album == nil || self.album!.id != album.id) { // Different album
            self.configureAlbum(album: album, track_index: track_index)
        }
        else if (track_index < 0) { // Shuffle
            self.stop()
            self.playQueue = self.createPlayQueue(num_tracks: self.album!.tracks.count, shuffle: true)
            self.current_track_index = 0
            self.registerCurrentTrackForTimeUpdates()
            self.play()
        }
        else if (currentTrack()?.id == self.album!.tracks[track_index].id) { // Same track
            self.play()
        }
        else {
            self.stop()
            self.playQueue = self.createPlayQueue(num_tracks: self.album!.tracks.count, shuffle: false)
            self.current_track_index = track_index
            self.registerCurrentTrackForTimeUpdates()
            self.play()
        }
        
        NotificationCenter.default.post(name: MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION, object: nil)
    }
    
    private func configureAlbum(album: Album, track_index: Int) {
        
        self.album = album
        
        if (self.album!.tracks.isEmpty) {
            // TODO: Error handling
            self.current_track_index = nil
            self.players = []
            self.playQueue = []
            return
        }
        
        // Configure playQueue
        if (track_index < 0) { // Shuffle
            self.playQueue = self.createPlayQueue(num_tracks: self.album!.tracks.count, shuffle: true)
            self.current_track_index = 0
        }
        else { // Track selected
            self.playQueue = self.createPlayQueue(num_tracks: self.album!.tracks.count, shuffle: false)
            self.current_track_index = track_index
        }
        
        // Configure players
        self.players = []
        for track in self.self.album!.tracks {
            let url = URL(string: track.track_url!)
            let player = AVPlayer(url: url!)
            self.players.append(player)
            
            // Register for end of track notification
            NotificationCenter.default.addObserver(self, selector: #selector(self.next), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        }
        
        self.registerCurrentTrackForTimeUpdates()
    }
    
    private func createPlayQueue(num_tracks: Int, shuffle: Bool) -> [Int] {
        
        var queue: [Int] = []
        for i in 0...(num_tracks-1) {
            queue.append(i)
        }
        
        if (!shuffle) {
            return queue
        }
        
        var shuffleQueue: [Int] = []
        while (queue.count > 0) {
            let randomIndex = Int(arc4random_uniform(UInt32(queue.count)))
            shuffleQueue.append(queue[randomIndex])
            queue.remove(at: randomIndex)
        }
        
        return shuffleQueue
    }
    
    private func loadDefaultAlbum() {
        
        let request = DefaultAlbumRequest()
        
        request.completionBlock = { (response, error) in
            
            if (self.album != nil) {
                // Album already selected
                return
            }
            
            if (error != nil) {
                // TODO: Error handling
                return
            }
            
            if let json = response {
                
                let defaultAlbum = Album()
                defaultAlbum.id = json["_id"].string
                defaultAlbum.title = json["title"].string
                defaultAlbum.subtitle = json["subtitle"].string
                
                if (json["cf_image_url"].string != nil) {
                    defaultAlbum.image_url = json["cf_image_url"].string
                } else {
                    defaultAlbum.image_url = json["image_url"].string
                }
                
                defaultAlbum.created_at = stringToDate(date: json["created_at"].stringValue)
                defaultAlbum.updated_at = stringToDate(date: json["updated_at"].stringValue)
                defaultAlbum.released = stringToDate(date: json["released"].stringValue)
                
                defaultAlbum.artist_title = json["artist_title"].string
                
                
                for trackData in json["tracks"].arrayValue {
                    
                    let track = Track()
                    track.id = trackData["_id"].string
                    track.title = trackData["title"].string
                    track.subtitle = trackData["subtitle"].string
                    
                    if (trackData["cf_track_url"].string != nil) {
                        track.track_url = trackData["cf_track_url"].string
                    } else {
                        track.track_url = trackData["track_url"].string
                    }
                    
                    track.created_at = stringToDate(date: trackData["created_at"].stringValue)
                    track.updated_at = stringToDate(date: trackData["updated_at"].stringValue)
                    
                    track.duration = trackData["duration"].int
                    track.track_num = trackData["track_num"].int
                    
                    defaultAlbum.tracks.append(track)
                    
                }
                
                self.prepareToPlay(album: defaultAlbum, track_index: 0)

            }
            
        }
        
        request.execute()
    }
    
    private func registerCurrentTrackForTimeUpdates() {
                
        self.currentTrackTimeObserver = self.currentPlayer()?.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.main, using: { (time) in
            
            var seconds = CMTimeGetSeconds(self.currentPlayer()!.currentTime())
            if (seconds < 0) {
                seconds = 0.0
            }
            
            self.playbackDelegate?.currentTrackPlaybackTimeChanged(currentTrackTime: Int(seconds))
        })
        
    }
    
    private func unregisterCurrentTrackForTimeUpdates() {
        
        if (self.currentTrackTimeObserver == nil) {
            return
        }
        
        self.currentPlayer()?.removeTimeObserver(self.currentTrackTimeObserver!)
    }
    
    func configureNowPlayingInfo() {
        
        if (self.currentPlayer() == nil || self.album == nil || self.currentTrack() == nil) {
            return
        }
        
        var nowPlayingInfo: [String:Any] = [:]
        
        if (self.album!.artist_title != nil) {
            nowPlayingInfo[MPMediaItemPropertyArtist] = self.album!.artist_title!
        }
        if (self.currentTrack()?.title != nil) {
            nowPlayingInfo[MPMediaItemPropertyTitle] = self.currentTrack()!.title!
        }
        if (self.album!.title != nil) {
            nowPlayingInfo[MPMediaItemPropertyAlbumTitle] = self.album!.title!
        }
        if (self.album!.image != nil) {
            nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: self.album!.image!.size, requestHandler: { (size) -> UIImage in
                return self.album!.image!
            })
        }
        if (self.currentTrackDuration() != nil) {
            nowPlayingInfo[MPMediaItemPropertyPlaybackDuration] = NSNumber(value: Double(self.currentTrackDuration()!))
        }
        
        let time = CMTimeGetSeconds(self.currentPlayer()!.currentTime())
        nowPlayingInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber(value: Double(time))
        
        nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = NSNumber(value: 1)
        
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
        
    }
    
    func enableRemoteCommands() {
        
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        MPRemoteCommandCenter.shared().previousTrackCommand.isEnabled = true;
        MPRemoteCommandCenter.shared().previousTrackCommand.addTarget(self, action: #selector(self.previous))
        
        MPRemoteCommandCenter.shared().nextTrackCommand.isEnabled = true
        MPRemoteCommandCenter.shared().nextTrackCommand.addTarget(self, action: #selector(self.next))
        
        MPRemoteCommandCenter.shared().playCommand.isEnabled = true
        MPRemoteCommandCenter.shared().playCommand.addTarget(self, action: #selector(self.play))
        
        MPRemoteCommandCenter.shared().pauseCommand.isEnabled = true
        MPRemoteCommandCenter.shared().pauseCommand.addTarget(self, action: #selector(self.pause))
    }
    
    func disableRemoteCommands() {
        
        UIApplication.shared.endReceivingRemoteControlEvents()
        
        MPRemoteCommandCenter.shared().previousTrackCommand.isEnabled = false;
        MPRemoteCommandCenter.shared().nextTrackCommand.isEnabled = false
        MPRemoteCommandCenter.shared().playCommand.isEnabled = false
        MPRemoteCommandCenter.shared().pauseCommand.isEnabled = false
    }
    
    
    // MARK: - Player State
    
    func currentTrack() -> Track? {
        
        if (self.album == nil || self.current_track_index == nil || self.playQueue.isEmpty) {
            return nil
        }
        
        return self.album!.tracks[self.playQueue[self.current_track_index!]]
    }
    
    private func currentPlayer() -> AVPlayer? {
        if (self.album == nil || self.current_track_index == nil || self.playQueue.isEmpty || self.players.isEmpty) {
            return nil
        }
        
        return self.players[self.playQueue[self.current_track_index!]]
    }
    
    func isPlaying() -> Bool {
        
        if (self.album == nil || self.current_track_index == nil || self.currentPlayer() == nil) {
            return false
        }
        
        return self.currentPlayer()!.rate > 0.0
    }
    
    func currentTrackTime() -> Int? {
        
        if (self.currentPlayer() == nil || self.currentPlayer()?.currentItem == nil) {
            return nil
        }
        
        if (self.currentPlayer()!.currentTime() == kCMTimeIndefinite) {
            return nil
        }
        
        return Int(CMTimeGetSeconds(self.currentPlayer()!.currentTime()))
    }
    
    func currentTrackDuration() -> Int? {
        
        if (self.currentPlayer() == nil || self.currentPlayer()?.currentItem == nil) {
            return nil
        }
        
        if (self.currentPlayer()!.currentItem!.duration == kCMTimeIndefinite) {
            return nil
        }
        
        var seconds = CMTimeGetSeconds(self.currentPlayer()!.currentItem!.duration)
        if (seconds < 0) {
            seconds = 0.0
        }
        
        return Int(seconds)
    }
    
    
    // MARK: - Player Controls
    
    func play() {
        
        if (self.currentPlayer() == nil || self.currentPlayer()!.currentItem == nil) {
            return
        }

        self.currentPlayer()?.play()
        
        self.configureNowPlayingInfo()
        
        NotificationCenter.default.post(name: MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION, object: nil)
    }
    
    func pause() {
        
        if (self.currentPlayer() == nil || self.currentPlayer()!.currentItem == nil) {
            return
        }
        
        self.currentPlayer()?.pause()
        
        self.configureNowPlayingInfo()
        
        NotificationCenter.default.post(name: MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION, object: nil)
    }
    
    func next() {
        
        if (self.album == nil || self.current_track_index == nil || self.playQueue.isEmpty) {
            return
        }
        
        self.stop()
        
        self.current_track_index! += 1
        
        if (self.current_track_index! >= self.playQueue.count) {
            self.current_track_index! = 0
        }
        
        self.currentPlayer()?.seek(to: kCMTimeZero)
        self.registerCurrentTrackForTimeUpdates()
        
        self.play()
        
        NotificationCenter.default.post(name: MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION, object: nil)
    }
    
    func previous() {
        
        if (self.album == nil || self.current_track_index == nil || self.playQueue.isEmpty) {
            return
        }
        
        self.stop()
        
        self.current_track_index! -= 1
        
        if (self.current_track_index! < 0) {
            self.current_track_index! = self.playQueue.count-1
        }
        
        self.currentPlayer()?.seek(to: kCMTimeZero)
        self.registerCurrentTrackForTimeUpdates()
        
        self.play()
        
        NotificationCenter.default.post(name: MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION, object: nil)
    }
    
    private func stop() {
        self.currentPlayer()?.pause()
        self.currentPlayer()?.seek(to: kCMTimeZero)
        self.unregisterCurrentTrackForTimeUpdates()
    }
    
    func seek(time: Float, completionBlock: ((Void) -> Void)?) {
        if (self.currentPlayer() == nil || self.currentPlayer()!.currentItem == nil) {
            return
        }
        
        self.currentPlayer()?.seek(to: CMTimeMakeWithSeconds(Float64(time), 10), completionHandler: { (finished) in
            
            self.configureNowPlayingInfo()
            
            if (completionBlock != nil) {
                completionBlock!()
            }
        })
        
    }
    

}
