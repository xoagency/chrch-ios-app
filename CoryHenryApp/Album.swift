//
//  Album.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/13/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class Album: NSObject {
    
    var id: String?
    var title: String?
    var subtitle: String?
    var image_url: String?
    var created_at: Date?
    var updated_at: Date?
    var artist_title: String?
    var released: Date?
    var tracks: [Track] = []
    var image: UIImage?
    
}
