//
//  TwitterViewController.swift
//  CoryHenryApp
//
//  Created by Rene Cabañas Lopez on 16/02/2017.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit
import TwitterKit

class TwitterViewController: TWTRTimelineViewController {
    
    var navBar: UINavigationBar = UINavigationBar()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setNavBarToTheView()
        setNeedsStatusBarAppearanceUpdate()
    
        dataSource = TWTRUserTimelineDataSource(screenName: "cory_henry", apiClient: TWTRAPIClient())
        TWTRTweetView.appearance().primaryTextColor = UIColor.white
        TWTRTweetView.appearance().backgroundColor = ACCENT_COLOR_BACKGROUND_CARD_TWITTER
        // Show Tweet actions
        self.showTweetActions = true
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.title = ""
        UINavigationBar.appearance().barTintColor = ACCENT_COLOR_NAVIGATION_BAR
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        UINavigationBar.appearance().isTranslucent = false;
        
        
        UITableView.appearance().separatorColor = COLOR_DIVIDER

        
        let font = UIFont(name: "HelveticaNeue-CondensedBold", size: 24)
        let leftBarButtonItem = UIBarButtonItem(title: "Twitter", style: .plain, target: self, action: #selector(addTapped))
        leftBarButtonItem.tintColor = UIColor.white
        leftBarButtonItem.setTitleTextAttributes([NSFontAttributeName:font ?? UIFont.boldSystemFont(ofSize: 24.0)], for: UIControlState.normal)
        leftBarButtonItem.isEnabled = true;
        navigationItem.leftBarButtonItem = leftBarButtonItem
        
        
        
        /*
        let rightBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(backView))
        let font2 = UIFont(name: "HelveticaNeue-Bold", size: 12)
        rightBarButtonItem.tintColor = ACCENT_COLOR_BUTTONS
        rightBarButtonItem.isEnabled = true;
        rightBarButtonItem.setTitleTextAttributes([NSFontAttributeName:font2 ?? UIFont.boldSystemFont(ofSize: 14.0)], for: UIControlState.normal)
        navigationItem.rightBarButtonItem = rightBarButtonItem
 */
        
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "DismissIcon"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        btn1.addTarget(self, action:  #selector(backView) , for: .touchUpInside)
        let rightBarButtonItem = UIBarButtonItem(customView: btn1)
        navigationItem.rightBarButtonItem = rightBarButtonItem
        

        // Do any additional setup after loading the view.
    }
    
    func setNavBarToTheView() {
        self.navBar.frame = CGRect(x:0,y:0,width:self.view.bounds.size.width, height:100)  // Here you can set you Width and Height for your navBar
        self.navBar.backgroundColor = ACCENT_COLOR_NAVIGATION_BAR
        self.view.addSubview(navBar)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTapped(){
    }
    
    func backView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame:CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        
        
        let viewDivider = UIView(frame:CGRect(x: 16, y: 15, width: self.view.bounds.size.width - 16, height: 1))
        viewDivider.backgroundColor = COLOR_DIVIDER
        view.addSubview(viewDivider)
        
        return view
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        
        return 30
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
