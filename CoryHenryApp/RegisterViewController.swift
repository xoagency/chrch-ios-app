//
//  RegisterViewController.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 1/2/17.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON
import Firebase
import FirebaseAuth


class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmTextField: UITextField!
    
    @IBOutlet weak var legalButton: UIButton!
    
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        // Configure text fields
        self.emailTextField.delegate = self
        self.emailTextField.text = nil
        self.emailTextField.textColor = UIColor.white
        self.emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSForegroundColorAttributeName : UIColor.darkGray])
        
        self.passwordTextField.delegate = self
        self.passwordTextField.text = nil
        self.passwordTextField.textColor = UIColor.white
        self.passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : UIColor.darkGray])
        
        self.confirmTextField.delegate = self
        self.confirmTextField.text = nil
        self.confirmTextField.textColor = UIColor.white
        self.confirmTextField.attributedPlaceholder = NSAttributedString(string: "Confirm password", attributes: [NSForegroundColorAttributeName : UIColor.darkGray])
        
        
        // Configure legal button
        self.legalButton.titleLabel?.textAlignment = .center
        
        
        // Dismiss keyboard on tap
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        self.view.addGestureRecognizer(tapRecognizer)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.dismissKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func dismissButtonAction(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func facebookButtonAction(_ sender: Any) {
        self.registerWithFacebook()
    }
    
    @IBAction func registerButtonAction(_ sender: Any) {
        self.register()
    }
    
    @IBAction func legalButtonAction(_ sender: Any) {
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: - Text Fields
    
    func dismissKeyboard() {
        self.emailTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
        self.confirmTextField.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField == self.emailTextField) {
            self.emailTextField.resignFirstResponder()
            self.passwordTextField.becomeFirstResponder()
        } else if (textField == self.passwordTextField) {
            self.passwordTextField.resignFirstResponder()
            self.confirmTextField.becomeFirstResponder()
        } else if (textField == self.confirmTextField) {
            self.confirmTextField.resignFirstResponder()
        }
        
        return true
    }
    
    
    
    // MARK: - Email/Password Registration
    
    func register() {
        
        self.dismissKeyboard()
        
        // Email validation
        if (self.emailTextField.text == nil || !isValidEmail(email: self.emailTextField.text!)) {
            let alert = UIAlertController(title: "Oops!", message: "Please enter a valid email address.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        // Password validation
        if (self.passwordTextField.text == nil || self.passwordTextField.text!.characters.count < 5) {
            let alert = UIAlertController(title: "Oops!", message: "Password must be at least 5 characters.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        // Confirm password validation
        if (self.confirmTextField.text == nil || self.confirmTextField.text! != self.passwordTextField.text!) {
            let alert = UIAlertController(title: "Oops!", message: "Passwords do not match.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        
        let email = self.emailTextField.text!
        let password = self.passwordTextField.text!
        
        
        FIRAuth.auth()?.createUser(withEmail:email, password:password) { (user, error) in
            
            if error == nil {
                print("You have successfully signed up")
                
                let userCurrent = User()
                
                userCurrent.id = user?.uid
                userCurrent.email = user?.email
                userCurrent.hasPassword = true
                
                let token = user?.uid
                
                // Update user state
                
                CURRENT_USER = userCurrent
                AUTH_TOKEN = token
                
                saveUserState()
                NotificationCenter.default.post(name: USER_STATE_CHANGED_NOTIFICATION, object: nil)
                
                // TODO: Push susbscription flow if necesary
                self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                
            } else {
               
                print("You have error signed up")
                
                
                let alert = UIAlertController(title: "Oops!", message: "\(error!)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return

            }
            
        }

        
        // Register
        /*
        let request = RegisterRequest(email: email, password: password)
        request.completionBlock = { (response, error) in
            self.onRegister(response: response, error: error)
        }
        request.execute()
         */
        
    }
    
    
    
    // MARK: - Facebook Registration
    
    func registerWithFacebook() {
        
        self.dismissKeyboard()
        
        let fbLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (loginResult: FBSDKLoginManagerLoginResult?, error: Error?) in
            
            if (error != nil || loginResult == nil) {
                
                print("Facebook login error: \(error)")
                
                let alert = UIAlertController(title: "Oops!", message: "Something went wrong. Please try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            else if (loginResult!.isCancelled) {
                print("Facebook login cancelled.")
                return
            }
            else {
                print("Facebook login successful.")
                
                FBSDKGraphRequest(graphPath: "/me", parameters: ["fields":"email, id"]).start(completionHandler: { (connection, result, error) in
                    
                    if let result = result as? NSDictionary {
                        
                        let email = result["email"] as? String
                        let id = result["id"] as? String
                        let token = FBSDKAccessToken.current().tokenString
                        
                        // Register
                        let request = RegisterFacebookRequest(email: email!, facebook_id: id!, facebook_token: token!)
                        request.completionBlock = { (response, error) in
                            self.onRegister(response: response, error: error)
                        }
                        request.execute()
                        
                    }
                    else {
                        
                        print("Facebook login error: \(error)")
                        
                        let alert = UIAlertController(title: "Oops!", message: "Something went wrong. Please try again.", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        return
                    }
                    
                })
                
                
            }
            
        }
        
    }
    
    
    
    // Mark: - Handle Response
    
    func onRegister(response: JSON?, error: Any?) {
        
        if (error != nil) {
            
            let alert = UIAlertController(title: "Oops!", message: "\(error!)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }

        
        // Parse response
        if let json = response {
            
            let user = User()
            if (json["user"] != JSON.null) {
                user.id = json["user"]["_id"].string
                user.email = json["user"]["email"].string
                user.facebook_id = json["user"]["facebook_id"].string
                if (json["user"]["password"] != JSON.null) {
                    user.hasPassword = true
                }
            }
            
            let token = json["token"].string
            
            // Error check
            if (user.id == nil || user.email == nil || token == nil) {
                let alert = UIAlertController(title: "Oops!", message: "Something went wrong. Please try again.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            
            // Update user state
            
            CURRENT_USER = user
            AUTH_TOKEN = token
            
            saveUserState()
            NotificationCenter.default.post(name: USER_STATE_CHANGED_NOTIFICATION, object: nil)
            
            // TODO: Push susbscription flow if necesary
            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            
        }
        
    }
    

}
