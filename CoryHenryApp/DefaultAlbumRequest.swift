//
//  DefaultAlbumRequest.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/24/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class DefaultAlbumRequest: BaseRequest {

    override func path() -> String {
        return "\(apiEndpointURL)/albums/default"
    }
    
}
