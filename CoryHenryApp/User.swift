//
//  User.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 1/4/17.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit

class User: NSObject {
    
    // NOTE: If user fields are added/removed, update saveUserState() and loadUserState() in Global.swift
    
    var id: String?
    var email: String?
    var facebook_id: String?
    var hasPassword: Bool = false

}
