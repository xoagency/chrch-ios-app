//
//  ChannelContentsRequest.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/13/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class ChannelContentsRequest: BaseRequest {
    
    var channel_id: String
    
    init(channel_id: String) {
        self.channel_id = channel_id
    }
    
    override func path() -> String {
        return "\(apiEndpointURL)/channels/\(self.channel_id)/contents"
    }

}
