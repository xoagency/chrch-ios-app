//
//  SettingsTableViewCell.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 1/4/17.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var title: String = "" {
        didSet {
            self.titleLabel.text = title
        }
    }
    
    var isButton: Bool = true {
        didSet {
            
            if (isButton) {
                self.titleLabel.textColor = UIColor.white
            }
            else {
                self.titleLabel.textColor = UIColor.darkGray
            }
            
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
