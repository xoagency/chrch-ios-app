//
//  ChannelCollectionViewCell.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/22/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class ChannelCollectionViewCell: UICollectionViewCell, NetworkImageViewDelegate {

    @IBOutlet weak var imageView: NetworkImageView!
    @IBOutlet weak var titleLabel: UILabel!

    var channel: Channel? = nil {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    func updateUI() {
        
        if (self.channel == nil) {
            return
        }
        if (self.channel!.title == nil) {
            self.titleLabel.text = "No disponible"
        }else{
            print(self.channel!.title ?? "")
            self.titleLabel.text = self.channel!.title
        }
        
        if (self.channel!.image == nil) {
            self.imageView.delegate = self
            self.imageView.image = UIImage(named:self.channel!.image_url!)
        } else {
            self.imageView.delegate = nil
            self.imageView.image = UIImage(named:self.channel!.image_url!)
        }
    }
    
    // MARK: - NetworkImageViewDelegate
    func networkImageLoaded(image: UIImage?) {
        //self.channel?.image = image
    }
    

}
