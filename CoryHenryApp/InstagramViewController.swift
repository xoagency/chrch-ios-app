//
//  InstagramViewController.swift
//  CoryHenryApp
//
//  Created by Rene Cabañas Lopez on 16/02/2017.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class InstagramViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var photoModels: [PhotoModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = ""
        UINavigationBar.appearance().barTintColor = ACCENT_COLOR_NAVIGATION_BAR
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        UINavigationBar.appearance().isTranslucent = false;
        
        let font = UIFont(name: "HelveticaNeue-CondensedBold", size: 30)
        let leftBarButtonItem = UIBarButtonItem(title: "Instagram", style: .plain, target: self, action: #selector(addTapped))
        leftBarButtonItem.tintColor = UIColor.white
        leftBarButtonItem.setTitleTextAttributes([NSFontAttributeName:font ?? UIFont.boldSystemFont(ofSize: 24.0)], for: UIControlState.normal)
        leftBarButtonItem.isEnabled = true;
        navigationItem.leftBarButtonItem = leftBarButtonItem
        
        let rightBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backView))
        let font2 = UIFont(name: "HelveticaNeue-Bold", size: 12)
        rightBarButtonItem.tintColor = ACCENT_COLOR_BUTTONS
        rightBarButtonItem.isEnabled = true;
        rightBarButtonItem.setTitleTextAttributes([NSFontAttributeName:font2 ?? UIFont.boldSystemFont(ofSize: 14.0)], for: UIControlState.normal)
        navigationItem.rightBarButtonItem = rightBarButtonItem
    
        
        tableView.dataSource = self
        tableView.delegate = self
        
        let client_id = "1535065703"
        let access_token = "1535065703.1677ed0.101d3141ac62488b829c3d47c0923c99"
        
        let url = URL(string: "https://api.instagram.com/v1/users/\(client_id)/media/recent/?access_token=\(access_token)")
        
        let request = URLRequest(url: url!)
        let session = URLSession(
            configuration: URLSessionConfiguration.default,
            delegate:nil,
            delegateQueue:OperationQueue.main
        )
        
        let task : URLSessionDataTask = session.dataTask(with: request,
                                                         completionHandler: { (dataOrNil, response, error) in
                                                            if let data = dataOrNil {
                                                                if let responseDictionary = try! JSONSerialization.jsonObject(
                                                                    with: data, options:[]) as? NSDictionary {
                                                                    
                                                                    print("---------------")
                                                                    
                                                                    print(responseDictionary)
                                                                    
                                                                    
                                                                    let photosArr: NSArray = (responseDictionary["data"] as? NSArray)!
                                                                    
                                                                    for photo in photosArr {
                                                                        self.photoModels.append(PhotoModel(json: (photo as? NSDictionary)!))
                                                                    }
                                                                    
                                                                    self.tableView.reloadData()
                                                                    
                                                                    
                                                                }
                                                            }
        });
        
        task.resume()
        
        //tableView.rowHeight = 320
        
    }
    
    @IBAction func dismisView(sender: AnyObject) {
        backView()
    }
    
    func addTapped(){
    }
    
    func backView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.photoModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath) as! PhotoCellView
        let photo = photoModels[indexPath.row]
        let photoUrl = URL(string: photo.url!)
        let photoPrfofileUrl = URL(string: photo.profilePicture!)
        
        
        cell.photoImageView.setImageWith(photoUrl!)
        cell.photoImageProfile.setImageWith(photoPrfofileUrl!)
        cell.textName.text = photo.username!
        cell.textNumberLikes.text = photo.countLikes!
        
        if photo.type == "image"{
            cell.iconPlayVide.isHidden = true
        }else if photo.type == "video"{
            cell.iconPlayVide.isHidden = false
        }
        
        return cell
    }
    
    /*
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        _ = sender as! UITableViewCell
        let destination = segue.destination as! PhotoDetailsViewController
        let indexPath = tableView.indexPath(for: sender as! UITableViewCell)
        let photo = photoModels[indexPath!.row]
        destination.photo = photo
    }
    */
    
    func tableView(_ _tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let photo = photoModels[indexPath.row]

        print(photo.type ?? "")
        if photo.type == "image"{
            //tableView.deselectRow(at: indexPath, animated: true)
        }else if photo.type == "video"{
            self.showVideo(urlVideo: photo.uriVideo!)
        }

        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame:CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        view.backgroundColor = ACCENT_COLOR_NAVIGATION_BAR
        
        let viewDivider = UIView(frame:CGRect(x: 16, y: 8, width: self.view.bounds.size.width - 16, height: 1))
        viewDivider.backgroundColor = COLOR_DIVIDER
        view.addSubview(viewDivider)
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        
        return 30
    }
 
 
    func showVideo(urlVideo : String) {
        let videoURL = NSURL(string: urlVideo)
        let player = AVPlayer(url: videoURL! as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }


}
