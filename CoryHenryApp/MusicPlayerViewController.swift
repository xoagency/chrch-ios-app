//
//  MusicPlayerViewController.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/21/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class MusicPlayerViewController: UIViewController, NetworkImageViewDelegate, MusicPlayerPlaybackDelegate {
    
    @IBOutlet weak var dismissButton: UIButton!
    
    @IBOutlet weak var imageView: NetworkImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    @IBOutlet weak var progressSlider: TBSlider!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    
    @IBOutlet weak var playPauseButton: UIButton!
    
    var isSliderTouched = false
    
    
    // MARK: - Configuration

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Configure default UI state
        self.imageView.image = nil
        self.titleLabel.text = ""
        self.subtitleLabel.text = ""
        self.currentTimeLabel.text = ""
        self.remainingTimeLabel.text = ""
        
        
        // Configure dismiss button
        self.dismissButton.setImage(UIImage(named: "DismissIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.dismissButton.tintColor = UIColor.darkGray
        
        
        // Configure progress slider
        self.progressSlider.setThumbImage(UIImage(named: "group2"), for: .normal)
        self.progressSlider.setThumbImage(UIImage(named: "group2"), for: .highlighted)
        
        self.progressSlider.addTarget(self, action: #selector(self.beginSliderTouch), for: .touchDown)
        self.progressSlider.addTarget(self, action: #selector(self.endSliderTouch), for: .touchUpInside)
        
        self.progressSlider.isContinuous = true
        self.progressSlider.addTarget(self, action: #selector(self.sliderValueChanged), for: .valueChanged)
        
        
        // Configure UI for music player
        if (MUSIC_PLAYER?.album != nil) {
            
            // Album art
            if (MUSIC_PLAYER?.album!.image == nil) {
                self.imageView.delegate = self
                //self.imageView.image_url = MUSIC_PLAYER?.album!.image_url
            } else {
                self.imageView.delegate = nil
                //self.imageView.image = MUSIC_PLAYER?.album!.image!
            }
            
             self.imageView.image  =  UIImage(named:"imgPlayAction")
            
            // Labels
            self.titleLabel.text = MUSIC_PLAYER?.currentTrack()?.title
            self.subtitleLabel.text = "\(MUSIC_PLAYER!.album!.artist_title!) — \(MUSIC_PLAYER!.album!.title!)"
            
            // Playback time
            if (MUSIC_PLAYER?.currentTrackTime() != nil) {
                self.currentTrackPlaybackTimeChanged(currentTrackTime: MUSIC_PLAYER!.currentTrackTime()!)
            } else {
                self.currentTrackPlaybackTimeChanged(currentTrackTime: 0)
            }
            
            // PlayPause Button
            if (MUSIC_PLAYER!.isPlaying()) {
                self.playPauseButton.setBackgroundImage(UIImage(named: "PauseIcon"), for: .normal)
            }
            else {
                self.playPauseButton.setBackgroundImage(UIImage(named: "PlayIcon"), for: .normal)
            }
            
        }
        
        
        // Register for music player updates
        MUSIC_PLAYER?.playbackDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.musicPlayerStateChanged), name: MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION, object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func dismissButtonAction(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - NetworkImageViewDelegate
    
    func networkImageLoaded(image: UIImage?) {
        MUSIC_PLAYER?.album?.image = image
    }
    
    
    // MARK: - Music Player State
    
    func musicPlayerStateChanged() {
        
        if (MUSIC_PLAYER?.album == nil) {
            
            // TODO: Error handling
            
            self.imageView.image = nil
            self.titleLabel.text = ""
            self.subtitleLabel.text = ""
            self.currentTimeLabel.text = ""
            self.remainingTimeLabel.text = ""
            return
        }
        
        // Album art
        if (MUSIC_PLAYER?.album!.image == nil) {
            //self.imageView.image_url = MUSIC_PLAYER?.album!.image_url
        } else {
           // self.imageView.image = MUSIC_PLAYER?.album!.image!
        }
        
        self.imageView.image  =  UIImage(named:"imgPlayAction")
        
        // Labels
        self.titleLabel.text = MUSIC_PLAYER?.currentTrack()?.title
        self.subtitleLabel.text = "\(MUSIC_PLAYER!.album!.artist_title!) — \(MUSIC_PLAYER!.album!.title!)"
        
        // PlayPause Button
        if (MUSIC_PLAYER!.isPlaying()) {
            self.playPauseButton.setBackgroundImage(UIImage(named: "PauseIcon"), for: .normal)
        }
        else {
            self.playPauseButton.setBackgroundImage(UIImage(named: "PlayIcon"), for: .normal)
        }
        
    }
    
    
    // MusicPlayerPlaybackDelegate
    func currentTrackPlaybackTimeChanged(currentTrackTime: Int) {
        
        if (!isSliderTouched) {
            self.updatePlaybackTimeLabels(time: currentTrackTime)
            self.updateProgressSliderForTime(time: currentTrackTime)
        }
        
        if (MUSIC_PLAYER?.currentTrackDuration() != nil && currentTrackTime == MUSIC_PLAYER!.currentTrackDuration()!) {
            MUSIC_PLAYER?.next()
        }
        
    }
    
    private func updatePlaybackTimeLabels(time: Int) {
        
        // Labels
        let current_min = time / 60
        let current_sec = time % 60
        var current_sec_padded = "\(current_sec)"
        if (current_sec < 10) {
            current_sec_padded = "0\(current_sec)"
        }
        self.currentTimeLabel.text = "\(current_min):\(current_sec_padded)"
        
        if (MUSIC_PLAYER!.currentTrackDuration() != nil) {
            let remaining = MUSIC_PLAYER!.currentTrackDuration()! - time
            let remaining_min = remaining / 60
            let remaining_sec = remaining % 60
            var remaining_sec_padded = "\(remaining_sec)"
            if (remaining_sec < 10) {
                remaining_sec_padded = "0\(remaining_sec)"
            }
            
            self.remainingTimeLabel.text = "-\(remaining_min):\(remaining_sec_padded)"
        }
        else {
            self.remainingTimeLabel.text = ""
        }
        
    }
    
    private func updateProgressSliderForTime(time: Int) {
        
        // Progress Slider
        self.progressSlider.minimumValue = 0.0
        
        if (MUSIC_PLAYER!.currentTrackDuration() != nil) {
            self.progressSlider.maximumValue = Float(MUSIC_PLAYER!.currentTrackDuration()!)
        } else {
            self.progressSlider.maximumValue = 360
        }
        
        
        if (Float(time) < self.progressSlider.value) {
            self.progressSlider.value = Float(time)
        } else {
            self.progressSlider.setValue(Float(time), animated: true)
        }
    }
    
    
    // MARK: - Seek
    
    func beginSliderTouch() {
        self.isSliderTouched = true
    }
    
    func endSliderTouch() {
        
        MUSIC_PLAYER?.seek(time: self.progressSlider.value, completionBlock: { (Void) in
            self.isSliderTouched = false
        })
        
        self.updatePlaybackTimeLabels(time: Int(self.progressSlider.value))
        self.updateProgressSliderForTime(time: Int(self.progressSlider.value))
    }
    
    func sliderValueChanged() {
        
        if (!isSliderTouched) {
            return
        }
        
        self.updatePlaybackTimeLabels(time: Int(self.progressSlider.value))
        
    }
    
    // MARK: - Actions
    
    @IBAction func playPauseButtonAction(_ sender: Any) {
        if (MUSIC_PLAYER == nil) {
            return
        }
        
        if (MUSIC_PLAYER!.isPlaying()) {
            MUSIC_PLAYER?.pause()
        }
        else {
            MUSIC_PLAYER?.play()
        }
        
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        MUSIC_PLAYER?.next()
    }
    
    @IBAction func previousButtonAction(_ sender: Any) {
        MUSIC_PLAYER?.previous()
    }
    
    

}


