//
//  ChildViewController.swift
//  CoryHenryApp
//
//  Created by Rene Cabañas Lopez on 21/02/2017.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Firebase
import FirebaseCore

class ChildViewController: AVPlayerViewController {
    
    /*
    @IBOutlet var customView:UIView!
    @IBOutlet var buttonPlay:UIButton!
    @IBOutlet var buttonPlayToolbar:UIButton!
    @IBOutlet var buttonNextToolbar:UIButton!
    @IBOutlet var labelDuration:UILabel!
    @IBOutlet var sliderDuration:TBSlider!
    @IBOutlet var controlsView:UIView!
     */

    var labelProDuration:UILabel!
    var sliderProDuration:TBSlider!
    var buttonGeneralPlay:UIButton!
    var buttonProPlay:UIButton!
    var buttonProNext:UIButton!
    var controlsProView:UIView!
    var isPlay : Bool!
    var url : String!

    override func viewDidLoad() {
        super.viewDidLoad()
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        let screenWidth  = UIScreen.main.bounds.width
        let screenHeight = UIScreen.main.bounds.height
        
        let contentView = UIView(frame: CGRect(x:0,y:0 ,width:screenHeight,height:screenWidth))
        contentView.backgroundColor = UIColor.clear
        
        let font = UIFont(name: "HelveticaNeue-CondensedBold", size: 16)
        let button = UIButton(frame: CGRect(x: screenHeight - 60 , y: 12, width: 46, height: 30))
        button.backgroundColor = UIColor.clear
        button.setTitle("Back", for: .normal)
        button.setTitleColor(ACCENT_COLOR_BUTTONS, for: UIControlState.normal)
        button.titleLabel!.font = font
        button.addTarget(self, action: #selector(ratingButtonTapped), for: .touchUpInside)
        contentView.addSubview(button)
        
        self.controlsProView = UIView(frame: CGRect(x:0,y:screenWidth-50 ,width:screenHeight,height:50))
        self.controlsProView.backgroundColor = UIColor.clear
        
        let image = UIImage(named:"rectangle2")
        let imageView = UIImageView(image: image!)
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.frame = CGRect(x: 0,y: 4 ,width:controlsProView.bounds.width,height:controlsProView.bounds.height)
        imageView.backgroundColor = UIColor.clear
        self.controlsProView.addSubview(imageView)
        
        self.labelProDuration = UILabel(frame: CGRect(x: controlsProView.bounds.width - 130, y: 18, width: 120, height: 21))
        self.labelProDuration.textColor = UIColor.white
        self.labelProDuration.textAlignment = .right
        self.controlsProView.addSubview(self.labelProDuration)
        
        self.sliderProDuration = TBSlider(frame:CGRect(x:100, y: 12, width: controlsProView.bounds.width - 230, height: 30))
        self.sliderProDuration.minimumValue = 0
        self.sliderProDuration.setThumbImage(UIImage(named: "group2"), for: UIControlState.normal)
        self.sliderProDuration.tintColor = ACCENT_COLOR_BUTTONS
        self.sliderProDuration.addTarget(self, action: #selector(ChildViewController.playbackSliderValueChanged(_:)), for: .valueChanged)
        self.controlsProView.addSubview(self.sliderProDuration)
        
    
        self.buttonProPlay = UIButton(frame: CGRect(x: 8 , y: 8, width: 40, height: 40))
        self.buttonProPlay.backgroundColor = UIColor.clear
        self.buttonProPlay.setTitle("", for: .normal)
        self.buttonProPlay.setImage(UIImage(named: "iconPause"), for: UIControlState.normal)
        self.buttonProPlay.addTarget(self, action: #selector(viewPlaying), for: .touchUpInside)
        self.controlsProView.addSubview(self.buttonProPlay)
        
        contentView.addSubview(controlsProView)
        
        self.buttonProNext = UIButton(frame: CGRect(x: 38 , y: 8, width: 50, height: 40))
        self.buttonProNext.backgroundColor = UIColor.clear
        self.buttonProNext.setTitle("", for: .normal)
        self.buttonProNext.setImage(UIImage(named: "iconSeek"), for: UIControlState.normal)
        self.buttonProNext.addTarget(self, action: #selector(viewPlaying), for: .touchUpInside)
        controlsProView.addSubview(self.buttonProNext)
        
        contentView.addSubview(controlsProView)
        
        buttonGeneralPlay = UIButton(frame: CGRect(x:screenHeight/2 - 31, y: screenWidth/2 - 31, width: 62, height: 62))
        buttonGeneralPlay.backgroundColor = UIColor.clear
        buttonGeneralPlay.setTitle("", for: .normal)
        buttonGeneralPlay.setImage(UIImage(named: "iconPauseOpen"), for: UIControlState.normal)
        buttonGeneralPlay.addTarget(self, action: #selector(viewPlaying), for: .touchUpInside)
        contentView.addSubview(buttonGeneralPlay)
        
        self.view.addSubview(contentView)
        
        
    
        //self.view.addSubview(customView)
        self.showsPlaybackControls = false
        play()
        isPlay = false
        let tapView = UITapGestureRecognizer(target: self, action:#selector(ChildViewController.handleTap(sender:)))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(tapView)
        //self.buttonPlay.setBackgroundImage(UIImage(named: "iconPauseOpen"), for: UIControlState.normal)
        //self.buttonPlayToolbar.setImage(UIImage(named: "iconPause"), for: UIControlState.normal)
        hideControls()
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateControls), userInfo: nil, repeats: true);
    
        // playbackSlider.addTarget(self, action: "playbackSliderValueChanged:", forControlEvents: .ValueChanged)
        // Do any additional setup after loading the view.
    }
    
    func ratingButtonTapped() {
        self.player?.pause()
        self.dismiss(animated: true, completion: nil)

    }
    
    func playbackSliderValueChanged(_ playbackSlider:TBSlider)
    {
        
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(seconds, 1)
        
        print(seconds)

        self.player!.seek(to: targetTime)
        
        playbackSlider.value = Float(seconds)
        
        if self.player!.rate == 0
        {
            self.player?.play()
        }
    }
    

    
    // must be internal or public.
    func updateControls() {
        // Something cool
        print("entro a monitorear")
        let currentItem = self.player?.currentItem
        
        let duration = currentItem?.asset.duration
        let currentTime = self.player?.currentTime().value
        
        print("duration")
        print(duration ?? "nada duracion")
        print("currentTime")
        print(currentTime ?? "No definido")
        
        let timeNow  = Int64((self.player?.currentTime().value)!) / Int64((self.player?.currentTime().timescale)!)
        
        let durationInSeconds = CMTimeGetSeconds(duration!)
    
        var strCurrentTime : String = ""
        var strDurationTime : String = ""
        secondsToHsMsSs(Int(durationInSeconds), result: { (h, m, s) in
            strDurationTime = "\(self.timeText(m)):\(self.timeText(s))" //\(self.timeText(h)):
            self.sliderProDuration.maximumValue = Float(s) + Float(m)*60

            
            
        })
        
        secondsToHsMsSs(Int(timeNow), result: { (h, m, s) in
            strCurrentTime = "\(self.timeText(m)):\(self.timeText(s))" //\(self.timeText(h)):
            self.sliderProDuration.value = Float(s)
        })
        
        self.labelProDuration.text = "\(strCurrentTime)/\(strDurationTime)"
        
    }
    
    func secondsToHsMsSs(_ seconds : Int, result: @escaping (Int, Int, Int)->()) {
        result(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func timeText(_ s: Int) -> String {
        return s < 10 ? "0\(s)" : "\(s)"
    }
    
    func handleTap(sender:UITapGestureRecognizer){
        // handling code
        viewPlaying()
    }
    
    func hideControls(){
        
        let when = DispatchTime.now() + 6
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.buttonGeneralPlay.isHidden = true
            self.controlsProView.isHidden = true
            self.buttonProPlay.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
 
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeRight
    }

    override var shouldAutorotate: Bool {
        
        return true
    }
    
    
    
    func play() {
        
        let videoURL = NSURL(string:self.url)
        let player = AVPlayer(url: videoURL! as URL)
    
        NotificationCenter.default.addObserver(self, selector: #selector(ChildViewController.didfinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        
        self.player = player
        
        self.player?.play()
        
    }
    
    func didfinishPlaying(note:NSNotification){
        self.player?.pause()
        self.dismiss(animated: true, completion: nil)
    }
    
   @IBAction func dismiss(sender:AnyObject){
        self.player?.pause()
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func playVideo(sender:AnyObject){
        viewPlaying()
    }
    
    
    func viewPlaying(){
        self.buttonGeneralPlay.isHidden = false
        self.controlsProView.isHidden = false
        self.buttonProPlay.isHidden = false
        if (isPlay == true) {
            isPlay = false
            self.player?.play()
            self.buttonGeneralPlay.setBackgroundImage(UIImage(named: "iconPauseOpen"), for: UIControlState.normal)
            self.buttonProPlay.setImage(UIImage(named: "iconPause"), for: UIControlState.normal)

        }else{
            isPlay = true
            self.buttonGeneralPlay.setBackgroundImage(UIImage(named: "iconPlayVideo"), for: UIControlState.normal)
            self.buttonProPlay.setImage(UIImage(named: "play"), for: UIControlState.normal)
            self.player?.pause()
        }
        hideControls()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
}


class TBSlider: UISlider {
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 16, width: bounds.size.width, height: 1)
    }
}
