//
//  SubscribeViewController.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 1/2/17.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit

class SubscribeViewController: UIViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    @IBOutlet weak var leftRestoreButton: UIButton!
    @IBOutlet weak var rightRestoreButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        leftRestoreButton.isHidden = true
        rightRestoreButton.isHidden = false
        // Do any additional setup after loading the view.
        
        // Configure background
        /*
        let random = Int(arc4random_uniform(UInt32(3)))
        switch random {
        case 0:
            backgroundImageView.image = UIImage(named: "BackgroundImage0")
            leftRestoreButton.isHidden = true
            rightRestoreButton.isHidden = false
        case 1:
            backgroundImageView.image = UIImage(named: "BackgroundImage1")
            leftRestoreButton.isHidden = false
            rightRestoreButton.isHidden = true
        case 2:
            backgroundImageView.image = UIImage(named: "BackgroundImage2")
            leftRestoreButton.isHidden = true
            rightRestoreButton.isHidden = false
        default:
            break
        }
 */
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - Actions
    
    @IBAction func dismissButtonAction(_ sender: Any) {
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }

}
