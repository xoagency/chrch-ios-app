//
//  PhotoDetailsViewController.swift
//  Instagram
//
//  Created by Satoru Sasozaki on 2/3/16.
//  Copyright © 2016 Satoru Sasozaki. All rights reserved.
//

import UIKit

class PhotosDetailsViewController : UIViewController, UIScrollViewDelegate {
    
    var imageURL : URL?
    var photoView : UIImageView?
    var scrollViewForZoom : UIScrollView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // set title
        self.navigationItem.title = "Details"
        self.view.backgroundColor = UIColor.white
        
        // set UIScrollView for zooming
        scrollViewForZoom = UIScrollView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        self.scrollViewForZoom?.minimumZoomScale = 1.0
        self.scrollViewForZoom?.maximumZoomScale = 6.0
        scrollViewForZoom?.delegate = self
        self.view.addSubview(scrollViewForZoom!)

        // Configure image
        photoView = PhotoView(frame: CGRect(x: 0, y: 75, width: self.view.frame.width, height: self.view.frame.width))
        photoView!.setImageWithUrl(imageURL!)
        self.scrollViewForZoom!.addSubview(photoView!)
    }
    
    // enable zooming
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.photoView
    }
}
