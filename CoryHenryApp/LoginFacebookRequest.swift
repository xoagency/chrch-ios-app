//
//  LoginFacebookRequest.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 1/1/17.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit
import Alamofire

class LoginFacebookRequest: BaseRequest {
    
    var facebook_id: String
    var facebook_token: String
    
    init(facebook_id: String, facebook_token: String) {
        self.facebook_id = facebook_id
        self.facebook_token = facebook_token
    }
    
    override func path() -> String {
        return "\(apiEndpointURL)/users/login/facebook"
    }
    
    override func method() -> HTTPMethod {
        return .post
    }
    
    override func params() -> [String : Any]? {
        var params: [String: Any] = [:]
        params["facebook_id"] = self.facebook_id
        params["facebook_token"] = self.facebook_token
        return params
    }

}
