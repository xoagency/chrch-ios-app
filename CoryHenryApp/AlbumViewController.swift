//
//  AlbumViewController.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/21/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class AlbumViewController: UITableViewController, NetworkImageViewDelegate {
    
    @IBOutlet weak var dismissButton: UIButton!
    
    @IBOutlet weak var imageView: NetworkImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var releasedLabel: UILabel!
    
    var item_type: Int? = nil
    var item_id: String? = nil
    var album: Album? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure default view states
        self.titleLabel.text = ""
        self.artistLabel.text = ""
        self.releasedLabel.text = ""
        

        // Configure dismiss button
        self.dismissButton.setImage(UIImage(named: "DismissIcon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        self.dismissButton.tintColor = UIColor.darkGray
        
        // Configure table view
        self.tableView.register(UINib(nibName: "TrackTableViewCell", bundle: nil), forCellReuseIdentifier: "TrackTableViewCell")
        self.tableView.register(UINib(nibName: "ShuffleTableViewCell", bundle: nil), forCellReuseIdentifier: "ShuffleTableViewCell")
        self.tableView.tableFooterView = UIView()
        self.tableView.separatorStyle = .none
        
        
        // Register for music player updates
        NotificationCenter.default.addObserver(self, selector: #selector(self.musicPlayerStateChanged), name: MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION, object: nil)
        
        
        // Load album
        self.loadAlbum()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func dismissButtonAction(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    

    // MARK: - Track table view

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (self.album != nil) {
            return self.album!.tracks.count + 1
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShuffleTableViewCell") as! ShuffleTableViewCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrackTableViewCell") as! TrackTableViewCell

        // Configure the cell...
        cell.track = self.album!.tracks[indexPath.row-1]
        
        if (MUSIC_PLAYER?.currentTrack() != nil && MUSIC_PLAYER?.currentTrack()?.id == cell.track?.id) {
            cell.isPlaying = true
        } else {
            cell.isPlaying = false
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        MUSIC_PLAYER?.prepareToPlay(album: self.album!, track_index: indexPath.row-1)
        MUSIC_PLAYER?.play()
        
        let musicPlayerViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MusicPlayerViewController") as! MusicPlayerViewController
        self.present(musicPlayerViewController, animated: true, completion: nil)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - Load Album
    
    func loadAlbum() {
        
        if (self.item_type == nil || self.item_id == nil) {
            return
        }
        
        
        let request = ContentItemRequest(item_type: self.item_type!, item_id: self.item_id!)
        
        request.completionBlock = { (response, error) in
            
            if (error != nil) {
                // TODO: Error handling
                return
            }
            
            if let json = response {
                
                self.album = Album()
                self.album!.id = json["_id"].string
                self.album!.title = json["title"].string
                self.album!.subtitle = json["subtitle"].string
                
                /*
                if (json["cf_image_url"].string != nil) {
                    self.album!.image_url = json["cf_image_url"].string
                } else {
                    self.album!.image_url = json["image_url"].string
                }
                */
                self.album!.created_at = stringToDate(date: json["created_at"].stringValue)
                self.album!.updated_at = stringToDate(date: json["updated_at"].stringValue)
                self.album!.released = stringToDate(date: json["released"].stringValue)
                
                self.album!.artist_title = json["artist_title"].string
                
                
                for trackData in json["tracks"].arrayValue {
                    
                    let track = Track()
                    track.id = trackData["_id"].string
                    track.title = trackData["title"].string
                    track.subtitle = trackData["subtitle"].string
                    
                    if (trackData["cf_track_url"].string != nil) {
                        track.track_url = trackData["cf_track_url"].string
                    } else {
                        track.track_url = trackData["track_url"].string
                    }
                    
                    track.created_at = stringToDate(date: trackData["created_at"].stringValue)
                    track.updated_at = stringToDate(date: trackData["updated_at"].stringValue)
                    
                    track.duration = trackData["duration"].int
                    track.track_num = trackData["track_num"].int
                    
                    self.album!.tracks.append(track)
                    
                }
                
                
                self.updateAlbumUI()
                self.tableView.reloadData()
            }
            
        }
        
        request.execute()
    }
    
    func updateAlbumUI() {
        
        if (self.album == nil) {
            return
        }
        
        self.titleLabel.text = self.album!.title!
        self.artistLabel.text = self.album!.artist_title!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        self.releasedLabel.text = dateFormatter.string(from: self.album!.released!)
        
        if (self.album!.image == nil) {
            self.imageView.delegate = self
            self.imageView.image_url = self.album!.image_url
        } else {
            self.imageView.delegate = nil
            self.imageView.image = self.album!.image!
        }
        
        self.imageView.image  =  UIImage(named:"imgPlayAction")
    }
    
    
    // MARK: - NetworkImageViewDelegate
    func networkImageLoaded(image: UIImage?) {
        self.album?.image = image
    }
    
    
    // MARK: - Music Player State
    
    func musicPlayerStateChanged() {
        
        if (MUSIC_PLAYER?.album == nil || MUSIC_PLAYER?.album?.id != self.album?.id) {
            return
        }
        
        for cell in self.tableView.visibleCells {
            if let trackCell = cell as? TrackTableViewCell {
                if (MUSIC_PLAYER?.currentTrack() != nil && MUSIC_PLAYER?.currentTrack()?.id == trackCell.track?.id) {
                    trackCell.isPlaying = true
                } else {
                    trackCell.isPlaying = false
                }
            }
        }
        
    }
    

}
































