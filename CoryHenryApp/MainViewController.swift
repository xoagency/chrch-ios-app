//
//  MainViewController.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/21/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import TwitterKit
import FirebaseDatabase
import Firebase
// MARK: - Global Vars


class MainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NetworkImageViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var settingsButton: UIButton!
    
    @IBOutlet weak var contentCollectionView: UICollectionView!
    @IBOutlet weak var channelCollectionView: UICollectionView!
    
    @IBOutlet weak var currentChannelTitleLabel: UILabel!
    @IBOutlet weak var currentChannelSubtitleLabel: UILabel!
    
    @IBOutlet weak var nowPlayingButton: UIButton!
    @IBOutlet weak var nowPlayingImageView: NetworkImageView!
    
    @IBOutlet weak var nowPlayingTitleLabel: UILabel!
    @IBOutlet weak var nowPlayingSubtitleLabel: UILabel!

    @IBOutlet weak var nowPlayingPlayPauseButton: UIButton!
    @IBOutlet weak var nowPlayingNextButton: UIButton!
    
    
    
    // MARK: - Vars
    
    var channels: [Channel] = []
    var currentChannelIndex = 0
    
    var channelContents: [Content] = []
    
    var isTwitter:Bool!
    var isInstagram:Bool!
    var typeChannel:String!
    
    var ref:FIRDatabaseReference!

    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        // Configure default view states
        self.currentChannelTitleLabel.text = ""
        self.currentChannelSubtitleLabel.text = ""
        self.contentCollectionView.backgroundColor = BACKGROUND_COLOR_MAIN
        self.channelCollectionView.backgroundColor = BACKGROUND_COLOR_MAIN
        
        self.nowPlayingImageView.isHidden = true
        self.nowPlayingTitleLabel.text = ""
        self.nowPlayingSubtitleLabel.text = ""
        self.nowPlayingPlayPauseButton.isHidden = true
        self.nowPlayingNextButton.isHidden = true
        
        
        // Configure channel collection view
        self.channelCollectionView.dataSource = self
        self.channelCollectionView.delegate = self
        self.channelCollectionView.register(UINib(nibName: "ChannelCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChannelCollectionViewCell")
        self.channelCollectionView.alwaysBounceHorizontal = true
        
        let channelLayout = UICollectionViewFlowLayout()
        channelLayout.itemSize = CGSize(width: 60, height:80)
        channelLayout.minimumLineSpacing = 8
        channelLayout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        channelLayout.scrollDirection = .horizontal
        
        self.channelCollectionView.setCollectionViewLayout(channelLayout, animated: false)
        
        
        
        
        // Configure content collection view
        self.contentCollectionView.dataSource = self
        self.contentCollectionView.delegate = self
        self.contentCollectionView.register(UINib(nibName: "ContentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ContentCollectionViewCell")
        self.contentCollectionView.alwaysBounceHorizontal = true
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        let contentLayout = UICollectionViewFlowLayout()
        let contentItemWidth = UIScreen.main.bounds.size.width * (self.contentCollectionView.frame.size.height - 42) / UIScreen.main.bounds.size.height // Proportional to screen size
        contentLayout.itemSize = CGSize(width: screenWidth/2, height: screenHeight/2)
        contentLayout.minimumLineSpacing = 8
        contentLayout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        contentLayout.scrollDirection = .horizontal
        
        self.contentCollectionView.setCollectionViewLayout(contentLayout, animated: false)
        
        
        // Initialize global music player
        MUSIC_PLAYER = MusicPlayer()
        
        // Register for music player updates
        NotificationCenter.default.addObserver(self, selector: #selector(self.musicPlayerStateChanged), name: MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION, object: nil)
        
        typeChannel = TYPE_WHATS
        // Load content
        self.loadChannels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    // MARK: - Actions
    
    @IBAction func settingsButtonAction(_ sender: Any) {
        
        let settingsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        
        if (self.navigationController != nil) {
            self.navigationController?.pushViewController(settingsViewController, animated: true)
        } else {
            self.present(settingsViewController, animated: true, completion: nil)
        }

    }
    
    @IBAction func nowPlayingButtonAction(_ sender: Any) {
        
        let musicPlayerViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MusicPlayerViewController") as! MusicPlayerViewController
        self.present(musicPlayerViewController, animated: true, completion: nil)
        
    }
    
    @IBAction func nowPlayingPlayPauseButtonAction(_ sender: Any) {
        if (MUSIC_PLAYER == nil) {
            return
        }
        
        if (MUSIC_PLAYER!.isPlaying()) {
            MUSIC_PLAYER?.pause()
        }
        else {
            MUSIC_PLAYER?.play()
        }
    }
    
    @IBAction func nowPlayingNextButtonAction(_ sender: Any) {
        MUSIC_PLAYER?.next()
    }
    
    
    
    // MARK: - Channel/Content Data
    
    func loadChannels() {
        
        self.channels = []
        self.channelCollectionView.reloadData()
        
        //Channels Local
        
        let channelWhats = Channel()
        channelWhats.type = TYPE_WHATS
        channelWhats.id = "585ce016e183a50011165dcc"
        channelWhats.title = "What’s Hap... "
        channelWhats.subtitle = "What's happening Cory Henry"
        channelWhats.image_url = "imgWhats"
        self.channels.append(channelWhats)
        
        let channelVideos = Channel()
        channelVideos.type = TYPE_VIDEO
        channelVideos.id = "2"
        channelVideos.title = "Videos"
        channelVideos.subtitle = "Videos Cory Henry"
        channelVideos.image_url = "imgVideos"
        self.channels.append(channelVideos)
        
        let channelEvents = Channel()
        channelEvents.type = TYPE_EVENT
        channelEvents.id = "3"
        channelEvents.title = "Events"
        channelEvents.subtitle = "Tours and Dates"
        channelEvents.image_url = "imgEvents"
        self.channels.append(channelEvents)
        
        let channelStore = Channel()
        channelStore.type = TYPE_STORE
        channelStore.id = "585ccd7db8b3760011a5586c"
        channelStore.title = "Store"
        channelStore.subtitle = "Store Cory Henry"
        channelStore.image_url = "imgStore"
        self.channels.append(channelStore)
        
        let channelAlbums = Channel()
        channelAlbums.type = TYPE_ALBUM
        channelAlbums.id = "585c8b6d0a402100111d9798"
        channelAlbums.title = "Albums"
        channelAlbums.subtitle = "Albums Cory Henry"
        channelAlbums.image_url = "imgAlbums"
        self.channels.append(channelAlbums)
        
        let channelSocial = Channel()
        channelSocial.type = TYPE_SOCIAL
        channelSocial.id = "6"
        channelSocial.title = "Social Media"
        channelSocial.subtitle = "Social Media from Cory Henry"
        channelSocial.image_url = "imgMedia"
        self.channels.append(channelSocial)
        
        let channelBlog = Channel()
        channelBlog.type = TYPE_BLOG
        channelBlog.id = "7"
        channelBlog.title = "Blog"
        channelBlog.subtitle = "By Cory Henry"
        channelBlog.image_url = "imgBlog"
        self.channels.append(channelBlog)
        
        self.channelCollectionView.reloadData()
        
        if (self.channels.count > 0) {
            self.selectChannel(index: 0)
        }
        
    }
    
    func selectChannel(index: Int) {
    
        self.currentChannelIndex = index
        self.currentChannelTitleLabel.text = self.channels[index].title
        self.currentChannelSubtitleLabel.text = self.channels[index].subtitle
        self.typeChannel = self.channels[index].type
        self.loadChannelContents(channel: self.channels[index])
    }
    
    func loadChannelContents(channel: Channel) {
        
        self.channelContents = []
        self.contentCollectionView.reloadData()
        
        if channel.type == TYPE_WHATS {
            
            let content2 = Content()
            content2.id = "Whats1"
            content2.title = "Last Concert In Miami"
            content2.subtitle = "August 12 2017"
            content2.image_url = ""
            content2.image_local = "imgWhats1"
            content2.premium = false
            content2.item_type = 0
            content2.item_id = ""
            self.channelContents.append(content2)
            
            let content = Content()
            content.id = "Whats1"
            content.title = "Interview With Cory Henry"
            content.subtitle = "August 13 2017"
            content.image_url = ""
            content.image_local = "imgWhats2"
            content.premium = false
            content.item_type = 0
            content.item_id = ""
            self.channelContents.append(content)
            
        }else if channel.type == TYPE_VIDEO {
            
            self.ref = FIRDatabase.database().reference()
            let videossRef = ref.child("videos")
            
            videossRef.observe(FIRDataEventType.value, with: { (snapshot) in
                let response = snapshot.value as! [NSString : AnyObject]
                
                for videosData in response {
                    
                    print("videosData")
                    print(videosData.value)
                    
                    let itemDict:NSDictionary = videosData.value as! NSDictionary
                    
                    let isAvailable = itemDict.value(forKey: "status") as? String

                    if(isAvailable == "published") {
                        
                        let content2 = Content()
                        content2.id = ""
                        content2.title = itemDict.value(forKey: "name") as? String
                        content2.subtitle = itemDict.value(forKey: "date") as? String
                        content2.image_url =  itemDict.value(forKey: "imageUrl") as? String
                        content2.image_local =  itemDict.value(forKey: "videoUrl") as? String
                        content2.premium = false
                        content2.item_type = 0
                        content2.item_id = ""
                        
                        self.channelContents.append(content2)
                    
                        
                    }
                    
                
                    
                }
                
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                self.contentCollectionView.reloadData()
                
            })
            
         }else if channel.type == TYPE_EVENT {
            
            let content2 = Content()
            content2.id = "Event 1"
            content2.title = "Live Act Cory Henry"
            content2.subtitle = "August 13 2017"
            content2.image_url = ""
            content2.image_local = "imgEvent1"
            content2.premium = false
            
            content2.item_type = 0
            content2.item_id = ""
            
            self.channelContents.append(content2)
            
            let content = Content()
            content.id = "Event 2"
            content.title = "The Revival Project"
            content.subtitle = "August 13 2017"
            content.image_url = ""
            content.image_local = "imgEvent2"
            content.premium = false
            
            content.item_type = 0
            content.item_id = ""
            
            self.channelContents.append(content)
            
        }else if channel.type == TYPE_STORE {
            
            
        }else if channel.type == TYPE_ALBUM {
            
            let request = ChannelContentsRequest(channel_id: channel.id!)
            
            request.completionBlock = { (response, error) in
                
                if (error != nil) {
                    // TODO: Error handling
                    return
                }
                
                if let json = response {
                    
                    // Parse contents
                    for contentData in json.arrayValue {
                        
                        let content = Content()
                        content.id = contentData["_id"].string
                        content.title = contentData["title"].string
                        content.subtitle = contentData["subtitle"].string
                        
                        if self.typeChannel == TYPE_ALBUM{
                            content.image_local = "imgAlbum1"
                        }
                        
                        
                        /*
                         if (contentData["cf_image_url"].string != nil) {
                         content.image_url = contentData["cf_image_url"].string
                         } else {
                         content.image_url = contentData["image_url"].string
                         }
                         */
                        
                        content.created_at = stringToDate(date: contentData["created_at"].stringValue)
                        content.updated_at = stringToDate(date: contentData["updated_at"].stringValue)
                        
                        content.premium = Bool(contentData["premium"].intValue)
                        
                        content.item_type = contentData["item_type"].int
                        content.item_id = contentData["item_id"].string
                        
                      
                        self.channelContents.append(content)
                    }
                    
                
                    
                    // Reload contents view
                    self.contentCollectionView.reloadData()
                }
                
                
            }
            
            request.execute()
            
        }else if channel.type == TYPE_SOCIAL {
            let content2 = Content()
            content2.id = "Instagram"
            content2.title = "View Instagram"
            content2.subtitle = ""
            content2.image_url = ""
            content2.typeMedia = TYPE_INSTAGRAM
            content2.image_local = "imgInstagram"
            content2.premium = false
            
            content2.item_type = 0
            content2.item_id = ""
            
            self.channelContents.append(content2)
            
            let content = Content()
            content.id = "Twitter"
            content.title = "View Twitter"
            content.subtitle = ""
            content.image_url = ""
            content.typeMedia = TYPE_TWITTER
            content.image_local = "imgTwitter"
            content.premium = false
            
            content.item_type = 0
            content.item_id = ""
            
            self.channelContents.append(content)
            
        }else if channel.type == TYPE_BLOG {
            
            let content2 = Content()
            content2.id = "Blog 1"
            content2.title = "Album Review 'The revival'"
            content2.subtitle = "February 7"
            content2.image_url = ""
            content2.image_local = "imgBlog1"
            content2.premium = false
            
            content2.item_type = 0
            content2.item_id = ""
            
            self.channelContents.append(content2)
        }
        
 
        self.contentCollectionView.reloadData()
    }
    
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (collectionView == self.channelCollectionView) {
            return self.channels.count
        } else {
            return self.channelContents.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == self.channelCollectionView) {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelCollectionViewCell", for: indexPath) as! ChannelCollectionViewCell
            
            cell.channel = self.channels[indexPath.row]
            
            if (indexPath.row == self.currentChannelIndex) {
                cell.alpha = 1.0
            } else {
                cell.alpha = 0.6
            }
            
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentCollectionViewCell", for: indexPath) as! ContentCollectionViewCell
           
            
            cell.content = self.channelContents[indexPath.row]
            cell.type = typeChannel
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (collectionView == self.channelCollectionView) {
        
            if (indexPath.row == self.currentChannelIndex) {
                return
            }
            
            let prevIndexPath = IndexPath(item: self.currentChannelIndex, section: 0)
            // collectionView.cellForItem(at: prevIndexPath)?.alpha = 0.8
            collectionView.cellForItem(at: indexPath)?.alpha = 1.0
            self.selectChannel(index: indexPath.row)
            collectionView.reloadItems(at: [prevIndexPath]) // Deselect previously selected cell (workaround)
            
            
        } else {
            
            let content = self.channelContents[indexPath.row]
            
            if typeChannel == TYPE_VIDEO {
                showVideo(url: content.image_local!)
            }else if typeChannel == TYPE_SOCIAL {
                if content.typeMedia == TYPE_TWITTER {
                    twitterButtonAction()
                }
                if content.typeMedia == TYPE_INSTAGRAM {
                    instagramButtonAction()
                }
            }else if typeChannel == TYPE_EVENT {
                eventButtonAction()
            }else if typeChannel == TYPE_BLOG {
                blogButtonAction()

            }else if typeChannel == TYPE_WHATS {
                
            }else{
                if (content.item_type == 0) { // Album
                    
                    let albumViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlbumViewController") as! AlbumViewController
                    albumViewController.item_type = content.item_type
                    albumViewController.item_id = content.item_id
                    self.present(albumViewController, animated: true, completion: nil)
                    
                    
                    //                 // Growing animation
                    //                let cell = collectionView.cellForItem(at: indexPath) as! ContentCollectionViewCell
                    //
                    //                let absOrigin = cell.convert(cell.imageView.frame.origin, to: self.view)
                    //
                    //                let animatingImageView = UIImageView(image: cell.imageView.image)
                    //                animatingImageView.frame = CGRect(x: absOrigin.x, y: absOrigin.y, width: cell.imageView.frame.size.width, height: cell.imageView.frame.size.height)
                    //                self.view.addSubview(animatingImageView)
                    //
                    //                let albumViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlbumViewController") as! AlbumViewController
                    //
                    //                UIView.animate(withDuration: 0.15, animations: {
                    //
                    //                    animatingImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                    //
                    //                }, completion: { (Bool) in
                    //
                    //                    self.present(albumViewController, animated: false, completion: {
                    //
                    //                        animatingImageView.removeFromSuperview()
                    //                    })
                    //
                    //                })
                    
                }

            }
        
        }
        
    }
    
    func showVideo(url:String) {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let videoController = storyBoard.instantiateViewController(withIdentifier: "ChildViewController") as! ChildViewController
        videoController.url = url
        self.present(videoController, animated: true, completion: nil)
    
        /*
        let videoURL = NSURL(string: "https://firebasestorage.googleapis.com/v0/b/chrches-bd376.appspot.com/o/CORY%20HENRY%20%20THE%20NEW%20CHAPTER.mp4?alt=media&token=2496db4b-5bda-49ad-9d1d-f783af9ff567")
        let player = AVPlayer(url: videoURL! as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
         */
    }
    
    // MARK: - NetworkImageViewDelegate
    func networkImageLoaded(image: UIImage?) {
        MUSIC_PLAYER?.album?.image = image
    }
    
    
    // MARK: - Music Player State
    
    func musicPlayerStateChanged() {
        
        if (MUSIC_PLAYER?.album == nil) {
            
            // TODO: Error handling
            
            self.nowPlayingImageView.isHidden = true
            self.nowPlayingTitleLabel.text = ""
            self.nowPlayingSubtitleLabel.text = ""
            self.nowPlayingPlayPauseButton.isHidden = true
            self.nowPlayingNextButton.isHidden = true
            
            return
        }
        
        self.nowPlayingImageView.isHidden = false
        self.nowPlayingPlayPauseButton.isHidden = false
        self.nowPlayingNextButton.isHidden = false
        
        // Album art
        if (MUSIC_PLAYER?.album!.image == nil) {
            self.nowPlayingImageView.delegate = self
            //self.nowPlayingImageView.image_url = MUSIC_PLAYER?.album!.image_url
        } else {
            self.nowPlayingImageView.delegate = nil
            //self.nowPlayingImageView.image = MUSIC_PLAYER?.album!.image!
        }
        
        // Labels
        self.nowPlayingTitleLabel.text = MUSIC_PLAYER?.currentTrack()?.title
        self.nowPlayingSubtitleLabel.text = "\(MUSIC_PLAYER!.album!.artist_title!) — \(MUSIC_PLAYER!.album!.title!)"
        
        // PlayPause Button
        if (MUSIC_PLAYER!.isPlaying()) {
            self.nowPlayingPlayPauseButton.setBackgroundImage(UIImage(named: "PauseIcon-LightGray"), for: .normal)
        }
        else {
            self.nowPlayingPlayPauseButton.setBackgroundImage(UIImage(named: "PlayIcon-LightGray"), for: .normal)
        }
        
    }
    
    func twitterButtonAction() {
        
        let nav : UINavigationController = UINavigationController(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "TwitterViewController") as! TwitterViewController)
        
        self.present(nav, animated: true, completion: nil)
        
        /*
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let twitterViewController = storyBoard.instantiateViewController(withIdentifier: "TwitterViewController")
        self.present(twitterViewController, animated: true, completion: nil)
 */
    }
    
    func instagramButtonAction(){
        
        /*
        let nav : UINavigationController = UINavigationController(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "InstagramViewController") as UIViewController)
        self.navigationController?.present(nav, animated: true, completion: nil)
        */
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let instagramController = storyBoard.instantiateViewController(withIdentifier: "InstagramViewController")
        self.present(instagramController, animated: true, completion: nil)
    }
    
    func eventButtonAction(){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let eventController = storyBoard.instantiateViewController(withIdentifier: "EventsViewController")
        self.present(eventController, animated: true, completion: nil)
    
    }
    
    func blogButtonAction(){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let eventController = storyBoard.instantiateViewController(withIdentifier: "BlogViewController")
        self.present(eventController, animated: true, completion: nil)
        
    }


}
