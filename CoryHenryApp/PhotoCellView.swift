//
//  PhotoCellView.swift
//  Instagram
//
//  Created by Isis Anchalee on 2/3/16.
//  Copyright © 2016 Isis Anchalee. All rights reserved.
//

import UIKit
import AFNetworking

class PhotoCellView: UITableViewCell {

    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var photoImageProfile: UIImageView!
    @IBOutlet weak var iconPlayVide: UIImageView!
    @IBOutlet weak var textName: UILabel!
    @IBOutlet weak var textNumberLikes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.photoImageProfile.layer.borderWidth = 1
        self.photoImageProfile.layer.masksToBounds = false
        self.photoImageProfile.layer.borderColor = UIColor.clear.cgColor
        self.photoImageProfile.layer.cornerRadius = self.photoImageProfile.frame.height/2
        self.photoImageProfile.clipsToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    /*
    func prepare(){
    

 }
 */
}
