//
//  Track.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/13/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class Track: NSObject {
    
    var id: String?
    var title: String?
    var subtitle: String?
    var track_url: String?
    var created_at: Date?
    var updated_at: Date?
    var duration: Int?
    var track_num: Int?

}
