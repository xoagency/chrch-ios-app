//
//  BlogViewController.swift
//  CoryHenryApp
//
//  Created by Rene Cabañas Lopez on 22/02/2017.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit

class BlogViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet var UITableView_tableBlog:UITableView!


    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.UITableView_tableBlog.delegate = self
        self.UITableView_tableBlog.dataSource = self
        self.UITableView_tableBlog.register(UINib(nibName: "BlogType1Cell", bundle: nil), forCellReuseIdentifier: "BlogType1Cell")
        self.UITableView_tableBlog.register(UINib(nibName: "BlogType2Cell", bundle: nil), forCellReuseIdentifier: "BlogType2Cell")
        self.UITableView_tableBlog.register(UINib(nibName: "BlogType3Cell", bundle: nil), forCellReuseIdentifier: "BlogType3Cell")
        self.UITableView_tableBlog.register(UINib(nibName: "BlogType4Cell", bundle: nil), forCellReuseIdentifier: "BlogType4Cell")
        
        self.UITableView_tableBlog.separatorStyle = .none
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismiss(sender:AnyObject){
        self.dismiss(animated: true, completion: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if indexPath.row == 0 {
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogType1Cell") as! BlogType1Cell
    
            return cell
        }else if indexPath.row == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogType2Cell") as! BlogType2Cell
            
            return cell
            
        }else if indexPath.row == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogType3Cell") as! BlogType3Cell
            
            return cell
            
        }else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "BlogType4Cell") as! BlogType4Cell
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if indexPath.row == 0 {
            return 471
        }else if indexPath.row == 1 {
            return 530
        }else if indexPath.row == 2 {
            return 663
        }else{
          return 681
        }
    }
    
    /*
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame:CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        
        
        let viewDivider = UIView(frame:CGRect(x: 16, y: 20, width: self.view.bounds.size.width - 16, height: 1))
        viewDivider.backgroundColor = COLOR_DIVIDER
        view.addSubview(viewDivider)
        
        return view
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        
        return 40
    }

 */
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
