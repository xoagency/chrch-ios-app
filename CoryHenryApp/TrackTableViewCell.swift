//
//  TrackTableViewCell.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/23/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class TrackTableViewCell: UITableViewCell {
    
    @IBOutlet weak var trackNumberLabel: UILabel!
    @IBOutlet weak var trackTitleLabel: UILabel!
    
    var track: Track? = nil {
        didSet {
            self.updateUI()
        }
    }
    
    var isPlaying: Bool = false {
        didSet {
            self.updateUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI() {
        
        if (self.track == nil) {
            return
        }
        
        self.trackNumberLabel.text = "\(self.track!.track_num!)"
        self.trackTitleLabel.text = self.track!.title!
        
        if (self.isPlaying) {
            self.trackNumberLabel.textColor = ACCENT_COLOR_MAIN
            self.trackTitleLabel.textColor = ACCENT_COLOR_MAIN
        }
        else {
            self.trackNumberLabel.textColor = UIColor.lightGray
            self.trackTitleLabel.textColor = UIColor.white
        }
        
    }
    
}
