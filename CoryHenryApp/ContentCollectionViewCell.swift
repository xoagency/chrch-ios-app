//
//  ContentCollectionViewCell.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/22/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class ContentCollectionViewCell: UICollectionViewCell, NetworkImageViewDelegate {
    
    @IBOutlet weak var imageView: NetworkImageView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var imagePlayVideo: UIImageView!
    @IBOutlet weak var imageIconSocial: UIImageView!
    @IBOutlet weak var viewGradient: UIView!


    var myColor: UIColor = .titleColor
    
    var content: Content? = nil {
        didSet {
            self.updateUI()
        }
    }
    
    var type: String? = nil {
        didSet {
            self.updateUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.clipsToBounds = false
        
        self.imageView.contentMode = .scaleAspectFill
        self.imageView.clipsToBounds = true
        
    }
    
    func updateUI() {
        
        if (self.content == nil) {
            return
        }
        
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.viewGradient.bounds
        
        // 3
        let color1 = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.0).cgColor as CGColor
        let color2 = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.0).cgColor as CGColor
        let color3 = UIColor(red: 0.0, green: 0, blue: 0, alpha: 0.0).cgColor as CGColor
        let color4 = UIColor(red: 0.0, green: 0, blue: 0, alpha: 1.0).cgColor as CGColor
        gradientLayer.colors = [color1, color2]
        
        // 4
        gradientLayer.locations = [0.0, 1.0]
        
        // 5
        self.viewGradient.layer.addSublayer(gradientLayer)
        
        self.imagePlayVideo.isHidden = true
        self.imageIconSocial.isHidden = true
        self.subtitleLabel.isHidden = false
        self.titleLabel.isHidden = false

        self.titleLabel.text = self.content?.title
        if type == TYPE_WHATS {
            self.subtitleLabel.text = self.content?.subtitle
            let imagebg = UIImage(named:(self.content?.image_local)!)
            self.imageView.image = imagebg
        }else if type == TYPE_VIDEO {
            self.subtitleLabel.text = self.content?.subtitle
            self.imagePlayVideo.isHidden = false
            if (self.content!.image == nil) {
                self.imageView.delegate = self
                self.imageView.image_url = self.content!.image_url
            } else {
                self.imageView.delegate = nil
                self.imageView.image = self.content!.image!
            }
        }else if type == TYPE_EVENT {
            self.subtitleLabel.text = self.content?.subtitle
            let imagebg = UIImage(named:(self.content?.image_local)!)
            self.imageView.image = imagebg
        }else if type == TYPE_STORE {
            self.subtitleLabel.text = self.content?.subtitle
            let imagebg = UIImage(named:(self.content?.image_local)!)
            self.imageView.image = imagebg
        }else if type == TYPE_ALBUM {
            self.subtitleLabel.text = self.content?.subtitle
            let imagebg = UIImage(named:"imgAlbums1")
            self.imageView.image = imagebg
        }else if type == TYPE_SOCIAL {
            self.subtitleLabel.isHidden = true
            self.imageIconSocial.isHidden = false
            if self.content?.typeMedia == TYPE_TWITTER{
                let image = UIImage(named:"iconTwitter")
                self.imageIconSocial.image = image
            }
            if self.content?.typeMedia == TYPE_INSTAGRAM{
                let image = UIImage(named:"instagramLogo")
                self.imageIconSocial.image = image
            }
            let imagebg = UIImage(named:(self.content?.image_local)!)
            self.imageView.image = imagebg
        }else if type == TYPE_BLOG {
            self.subtitleLabel.text = self.content?.subtitle
            let imagebg = UIImage(named:(self.content?.image_local)!)
            self.imageView.image = imagebg

        }
  
        
     
    
    }
    
    // MARK: - NetworkImageViewDelegate
    func networkImageLoaded(image: UIImage?) {
        self.content?.image = image
    }

}

extension UIColor
{
    public class var titleColor: UIColor
    {
        return UIColor(red: 72/255, green: 188/255, blue: 201/255, alpha: 1.0)
    }
}
