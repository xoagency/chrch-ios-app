//
//  VideoMainViewController.swift
//  CoryHenryApp
//
//  Created by Rene Cabañas Lopez on 11/02/2017.
//  Copyright © 2017 Crown & Lantern. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class VideoMainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, NetworkImageViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var settingsButton: UIButton!
    
    @IBOutlet weak var contentCollectionView: UICollectionView!
    @IBOutlet weak var channelCollectionView: UICollectionView!
    
    @IBOutlet weak var currentChannelTitleLabel: UILabel!
    @IBOutlet weak var currentChannelSubtitleLabel: UILabel!
    
    @IBOutlet weak var nowPlayingButton: UIButton!
    @IBOutlet weak var nowPlayingImageView: NetworkImageView!
    
    @IBOutlet weak var nowPlayingTitleLabel: UILabel!
    @IBOutlet weak var nowPlayingSubtitleLabel: UILabel!
    
    @IBOutlet weak var nowPlayingPlayPauseButton: UIButton!
    @IBOutlet weak var nowPlayingNextButton: UIButton!
    
    
    
    // MARK: - Vars
    
    var channels: [Channel] = []
    var currentChannelIndex = 0
    
    var channelContents: [Content] = []
    
    
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        // Configure default view states
        self.currentChannelTitleLabel.text = ""
        self.currentChannelSubtitleLabel.text = ""
        self.contentCollectionView.backgroundColor = BACKGROUND_COLOR_MAIN
        self.channelCollectionView.backgroundColor = BACKGROUND_COLOR_MAIN
        
        self.nowPlayingImageView.isHidden = true
        self.nowPlayingTitleLabel.text = ""
        self.nowPlayingSubtitleLabel.text = ""
        self.nowPlayingPlayPauseButton.isHidden = true
        self.nowPlayingNextButton.isHidden = true
        
        
        // Configure channel collection view
        self.channelCollectionView.dataSource = self
        self.channelCollectionView.delegate = self
        self.channelCollectionView.register(UINib(nibName: "ChannelCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ChannelCollectionViewCell")
        self.channelCollectionView.alwaysBounceHorizontal = true
        
        let channelLayout = UICollectionViewFlowLayout()
        channelLayout.itemSize = CGSize(width: self.channelCollectionView.frame.size.height, height: self.channelCollectionView.frame.size.height)
        channelLayout.minimumLineSpacing = 10
        channelLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        channelLayout.scrollDirection = .horizontal
        
        self.channelCollectionView.setCollectionViewLayout(channelLayout, animated: false)
        
        
        // Configure content collection view
        self.contentCollectionView.dataSource = self
        self.contentCollectionView.delegate = self
        self.contentCollectionView.register(UINib(nibName: "ContentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ContentCollectionViewCell")
        self.contentCollectionView.alwaysBounceHorizontal = true
        
        let contentLayout = UICollectionViewFlowLayout()
        let contentItemWidth = UIScreen.main.bounds.size.width * (self.contentCollectionView.frame.size.height - 42) / UIScreen.main.bounds.size.height // Proportional to screen size
        contentLayout.itemSize = CGSize(width: contentItemWidth, height: self.contentCollectionView.frame.size.height)
        contentLayout.minimumLineSpacing = 16
        contentLayout.sectionInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        contentLayout.scrollDirection = .horizontal
        
        self.contentCollectionView.setCollectionViewLayout(contentLayout, animated: false)
        
        
        // Initialize global music player
        MUSIC_PLAYER = MusicPlayer()
        
        // Register for music player updates
        NotificationCenter.default.addObserver(self, selector: #selector(self.musicPlayerStateChanged), name: MUSIC_PLAYER_STATE_CHANGED_NOTIFICATION, object: nil)
        
        
        // Load content
        self.loadChannels()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    // MARK: - Actions
    
    @IBAction func settingsButtonAction(_ sender: Any) {
        
        let settingsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        
        if (self.navigationController != nil) {
            self.navigationController?.pushViewController(settingsViewController, animated: true)
        } else {
            self.present(settingsViewController, animated: true, completion: nil)
        }
        
    }
    
    func showVideo() {
        let videoURL = NSURL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        let player = AVPlayer(url: videoURL! as URL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @IBAction func nowPlayingVideoButtonAction(_ sender: Any) {
        showVideo()
    }
    
    @IBAction func nowPlayingButtonAction(_ sender: Any) {
        showVideo()
        /*
        let musicPlayerViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MusicPlayerViewController") as! MusicPlayerViewController
        self.present(musicPlayerViewController, animated: true, completion: nil)
        */
       
    }
    
    @IBAction func nowPlayingPlayPauseButtonAction(_ sender: Any) {
        showVideo()
        /*
        if (MUSIC_PLAYER == nil) {
            return
        }
        
        if (MUSIC_PLAYER!.isPlaying()) {
            MUSIC_PLAYER?.pause()
        }
        else {
            MUSIC_PLAYER?.play()
        }
        */
    }
    
    @IBAction func nowPlayingNextButtonAction(_ sender: Any) {
        //MUSIC_PLAYER?.next()
        showVideo()
    }
    
    
    
    // MARK: - Channel/Content Data
    
    func loadChannels() {
        
        self.channels = []
        self.channelCollectionView.reloadData()
        
        let request = ChannelsRequest()
        
        request.completionBlock = { (response, error) in
            
            if (error != nil) {
                // TODO: Error handling
                return
            }
            
            if let json = response {
                
                // Parse channels
                for channelData in json.arrayValue {
                    
                    let channel = Channel()
                    channel.id = channelData["_id"].string
                    channel.title = channelData["title"].string
                    channel.subtitle = channelData["subtitle"].string
                    
                    if (channelData["cf_image_url"].string != nil) {
                        channel.image_url = channelData["cf_image_url"].string
                    } else {
                        channel.image_url = channelData["image_url"].string
                    }
                    
                    channel.created_at = stringToDate(date: channelData["created_at"].stringValue)
                    channel.updated_at = stringToDate(date: channelData["updated_at"].stringValue)
                    
                    self.channels.append(channel)
                    
                }
                
                // Load initial channel
                if (self.channels.count > 0) {
                    self.selectChannel(index: 0)
                }
                
                // Reload channels view
                self.channelCollectionView.reloadData()
            }
            
        }
        
        request.execute()
    }
    
    
    func selectChannel(index: Int) {
        
        self.currentChannelIndex = index
        
        self.currentChannelTitleLabel.text = self.channels[index].title
        self.currentChannelSubtitleLabel.text = self.channels[index].subtitle
        
        self.loadChannelContents(channel: self.channels[index])
    }
    
    func loadChannelContents(channel: Channel) {
        
        self.channelContents = []
        self.contentCollectionView.reloadData()
        
        let request = ChannelContentsRequest(channel_id: channel.id!)
        
        request.completionBlock = { (response, error) in
            
            if (error != nil) {
                // TODO: Error handling
                return
            }
            
            if let json = response {
                
                // Parse contents
                for contentData in json.arrayValue {
                    
                    let content = Content()
                    content.id = contentData["_id"].string
                    content.title = contentData["title"].string
                    content.subtitle = contentData["subtitle"].string
                    
                    if (contentData["cf_image_url"].string != nil) {
                        content.image_url = contentData["cf_image_url"].string
                    } else {
                        content.image_url = contentData["image_url"].string
                    }
                    
                    content.created_at = stringToDate(date: contentData["created_at"].stringValue)
                    content.updated_at = stringToDate(date: contentData["updated_at"].stringValue)
                    
                    content.premium = Bool(contentData["premium"].intValue)
                    
                    content.item_type = contentData["item_type"].int
                    content.item_id = contentData["item_id"].string
                    
                    self.channelContents.append(content)
                }
                
                // Reload contents view
                self.contentCollectionView.reloadData()
            }
            
        }
        
        request.execute()
    }
    
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (collectionView == self.channelCollectionView) {
            return self.channels.count
        } else {
            return self.channelContents.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == self.channelCollectionView) {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelCollectionViewCell", for: indexPath) as! ChannelCollectionViewCell
            
            cell.channel = self.channels[indexPath.row]
            
            
            
            if (indexPath.row == self.currentChannelIndex) {
                cell.alpha = 1.0
            } else {
                cell.alpha = 0.8
            }
            
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContentCollectionViewCell", for: indexPath) as! ContentCollectionViewCell
            cell.content = self.channelContents[indexPath.row]
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showVideo()
        /*
        if (collectionView == self.channelCollectionView) {
            
            if (indexPath.row == self.currentChannelIndex) {
                return
            }
            
            let prevIndexPath = IndexPath(item: self.currentChannelIndex, section: 0)
            // collectionView.cellForItem(at: prevIndexPath)?.alpha = 0.8
            collectionView.cellForItem(at: indexPath)?.alpha = 1.0
            self.selectChannel(index: indexPath.row)
            collectionView.reloadItems(at: [prevIndexPath]) // Deselect previously selected cell (workaround)
            
            
        } else {
            
            let content = self.channelContents[indexPath.row]
            
            if (content.item_type == 0) { // Album
                
                let albumViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlbumViewController") as! AlbumViewController
                albumViewController.item_type = content.item_type
                albumViewController.item_id = content.item_id
                self.present(albumViewController, animated: true, completion: nil)
                
                
                //                 // Growing animation
                //                let cell = collectionView.cellForItem(at: indexPath) as! ContentCollectionViewCell
                //
                //                let absOrigin = cell.convert(cell.imageView.frame.origin, to: self.view)
                //
                //                let animatingImageView = UIImageView(image: cell.imageView.image)
                //                animatingImageView.frame = CGRect(x: absOrigin.x, y: absOrigin.y, width: cell.imageView.frame.size.width, height: cell.imageView.frame.size.height)
                //                self.view.addSubview(animatingImageView)
                //
                //                let albumViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AlbumViewController") as! AlbumViewController
                //
                //                UIView.animate(withDuration: 0.15, animations: {
                //
                //                    animatingImageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                //
                //                }, completion: { (Bool) in
                //
                //                    self.present(albumViewController, animated: false, completion: {
                //
                //                        animatingImageView.removeFromSuperview()
                //                    })
                //
                //                })
                
            }
            
        }
        */
        
    }
    
    
    // MARK: - NetworkImageViewDelegate
    func networkImageLoaded(image: UIImage?) {
        MUSIC_PLAYER?.album?.image = image
    }
    
    
    // MARK: - Music Player State
    
    func musicPlayerStateChanged() {
        
        if (MUSIC_PLAYER?.album == nil) {
            
            // TODO: Error handling
            
            self.nowPlayingImageView.isHidden = true
            self.nowPlayingTitleLabel.text = ""
            self.nowPlayingSubtitleLabel.text = ""
            self.nowPlayingPlayPauseButton.isHidden = true
            self.nowPlayingNextButton.isHidden = true
            
            return
        }
        
        self.nowPlayingImageView.isHidden = false
        self.nowPlayingPlayPauseButton.isHidden = false
        self.nowPlayingNextButton.isHidden = false
        
        // Album art
        if (MUSIC_PLAYER?.album!.image == nil) {
            self.nowPlayingImageView.delegate = self
            self.nowPlayingImageView.image_url = MUSIC_PLAYER?.album!.image_url
        } else {
            self.nowPlayingImageView.delegate = nil
            self.nowPlayingImageView.image = MUSIC_PLAYER?.album!.image!
        }
        
        // Labels
        self.nowPlayingTitleLabel.text = MUSIC_PLAYER?.currentTrack()?.title
        self.nowPlayingSubtitleLabel.text = "\(MUSIC_PLAYER!.album!.artist_title!) — \(MUSIC_PLAYER!.album!.title!)"
        
        // PlayPause Button
        if (MUSIC_PLAYER!.isPlaying()) {
            self.nowPlayingPlayPauseButton.setBackgroundImage(UIImage(named: "PauseIcon-LightGray"), for: .normal)
        }
        else {
            self.nowPlayingPlayPauseButton.setBackgroundImage(UIImage(named: "PlayIcon-LightGray"), for: .normal)
        }
        
    }
    
    
}

