//
//  Channel.swift
//  CoryHenryApp
//
//  Created by Rishi Mody on 12/13/16.
//  Copyright © 2016 Crown & Lantern. All rights reserved.
//

import UIKit

class Channel: NSObject {
    var type: String?
    var id: String?
    var title: String?
    var subtitle: String?
    var image_url: String?
    var created_at: Date?
    var updated_at: Date?
    var contents: [Content] = []
    var image: UIImage?
}
